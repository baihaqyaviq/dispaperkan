<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

// menu1
Route::get('/visi', function () {
    return view('visi');
});
Route::get('/misi', function () {
    return view('/menu2');
});
Route::get('/fungsi', function () {
    return view('/menu2');
});
Route::get('/hukum', function () {
    return view('/menu2');
});
Route::get('/tupoksi', function () {
    return view('/menu2');
});
Route::get('/sekretariat', function () {
    return view('/menu2');
});
Route::get('/bidang', function () {
    return view('/menu2');
});
Route::get('/upt', function () {
    return view('/menu2');
});
Route::get('/personil', function () {
    return view('/menu2');
});
Route::get('/contactus', function () {
    return view('/menu2');
});

// menu2
Route::get('/daftarinformasipublik', function () {
    return view('/daftarinformasipublik');
});
Route::get('/profilppid', function () {
    return view('/profilppid');
});
Route::get('/skppid', function () {
    return view('/skppid');
});
Route::get('/contactus', function () {
    return view('/contactus');
});

//menu 3
Route::get('/renstra', function () {
    return view('/renstra');
});
Route::get('/renja', function () {
    return view('/renja');
});
Route::get('/dpa', function () {
    return view('/dpa');
});
Route::get('/lkjip', function () {
    return view('/lkjip');
});
Route::get('/lracalk', function () {
    return view('/lracalk');
});
Route::get('/pobl', function () {
    return view('/pobl');
});
Route::get('/laporanaset', function () {
    return view('/laporanaset');
});

//menu 4 
Route::get('/kartutani', function () {
    return view('/kartutani');
});

//menu 5
Route::get('/datatanaman', function () {
    return view('/datatanaman');
});

//menu 6 
Route::get('/kelembagaanpetani', function () {
    return view('/kelembagaanpetani');
});
Route::get('/posluhdes', function () {
    return view('/posluhdes');
});
