<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Repositories\KategoriRepository;

class BasisDataController extends ModuleController
{
    protected $moduleName = 'basisDatas';

    protected $permalinkBase = 'basis-data';

    protected $formWith = [
        'kategori'
    ];

    protected $indexWith = [
        'kategori'
    ];

    protected function indexData($request)
    {
        return [
            'categoryList' => app(KategoriRepository::class)->listAll('title'),
        ];
    }

    public function formData($request){
        return [
            'categoryList' => app(KategoriRepository::class)->listAll('title'),
        ];
    }
}
