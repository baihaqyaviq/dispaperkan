<?php

namespace App\Models;


use A17\Twill\Models\Model;

class Kategori extends Model
{


    protected $fillable = [
        'published',
        'title',
        'description',
    ];

    public function basisData()
    {
        return $this->hasMany(BasisData::class, 'category_id');
    }

}
