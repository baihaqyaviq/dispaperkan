<?php

namespace App\Models;


use A17\Twill\Models\Model;

class BasisData extends Model
{


    protected $fillable = [
        'published',
        'title',
        'description',
        'category_id',
        'year',
        'data',
    ];

    protected $casts = [
        'data' => 'array'
    ];

    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'category_id');
    }

}
