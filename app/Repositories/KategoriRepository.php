<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\Kategori;

class KategoriRepository extends ModuleRepository
{
    

    public function __construct(Kategori $model)
    {
        $this->model = $model;
    }
}
