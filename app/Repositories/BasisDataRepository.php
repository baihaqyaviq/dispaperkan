<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\BasisData;

class BasisDataRepository extends ModuleRepository
{
    

    public function __construct(BasisData $model)
    {
        $this->model = $model;
    }
}
