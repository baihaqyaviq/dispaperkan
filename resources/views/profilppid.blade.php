@extends('layout.master')
@section('content')

    <!-- Promo Block -->
    <section class="container g-py-150">
        <div class="row">
            <div class="col-lg-6">
                <div class="d-inline-block g-width-80 g-height-4 g-bg-black mb-3"></div>
                <h2 class="g-color-black g-font-weight-700 g-font-size-50 g-line-height-1 mb-4">Profil PPID</h2>
                <p class="mb-0">
                    DINAS PANGAN PERTANIAN DAN PERIKANAN KABUPATEN WONOSOBO
                    <br> PEMERINTAH KABUPATEN WONOSOBO
                </p>
            </div>
        </div>
    </section>
    <!-- End Promo Block -->

    <!-- Content -->
    <section class="g-bg-gray-light-v5">
        <div class="container g-py-100">
            <div class="row">
                <div id="stickyblock-start" class="col-md-4 g-mb-30">
                    <div class="js-sticky-block g-sticky-block--lg" data-type="responsive"
                        data-start-point="#stickyblock-start" data-end-point="#stickyblock-end">
                        <ul class="list-unstyled mb-0">
                            <li class="g-bg-gray-dark-v2 g-rounded-top-5 g-px-20 g-pt-30">
                                <a class="js-go-to d-block g-color-white g-bg-primary--hover g-font-weight-600 g-font-size-12 text-uppercase g-text-underline--none--hover rounded g-px-20 g-py-15"
                                    href="#" data-target="#license">Alamat PPID</a>
                            </li>
                            <li class="g-bg-gray-dark-v2 g-px-20">
                                <a class="js-go-to d-block g-color-white g-bg-primary--hover g-font-weight-600 g-font-size-12 text-uppercase g-text-underline--none--hover rounded g-px-20 g-py-15"
                                    href="#" data-target="#ownership">Struktur PPID Dispaperkan</a>
                            </li>
                            <li class="g-bg-gray-dark-v2 g-rounded-bottom-5 g-px-20 g-pb-30">
                                <a class="js-go-to d-block g-color-white g-bg-primary--hover g-font-weight-600 g-font-size-12 text-uppercase g-text-underline--none--hover rounded g-px-20 g-py-15"
                                    href="#" data-target="#usage">Tugas PPID</a>
                            </li>
                            {{-- <li class="g-bg-gray-dark-v2 g-rounded-bottom-5 g-px-20 g-pb-30">
                                <a class="js-go-to d-block g-color-white g-bg-primary--hover g-font-weight-600 g-font-size-12 text-uppercase g-text-underline--none--hover rounded g-px-20 g-py-15"
                                    href="#" data-target="#refunds">Refunds</a>
                            </li> --}}
                        </ul>
                    </div>
                </div>

                <div class="col-md-8">
                    <!-- License -->
                    <div id="license" class="u-shadow-v19 g-bg-white rounded g-pa-40 g-mb-50">
                        <h2 class="h3 g-color-black mb-3">Alamat PPID</h2>
                        <div class="d-inline-block g-width-50 g-height-2 g-bg-black mb-3"></div>

                        <p style="text-align: center;"><a
                                href="https://dispaperkan.wonosobokab.go.id/wp-content/uploads/2018/10/cropped-1200px-Kabupaten_Wonosobo.png"><img
                                    class="aligncenter wp-image-3225 "
                                    src="https://dispaperkan.wonosobokab.go.id/wp-content/uploads/2018/10/cropped-1200px-Kabupaten_Wonosobo-267x300.png"
                                    alt="" width="213" height="239"
                                    srcset="https://dispaperkan.wonosobokab.go.id/wp-content/uploads/2018/10/cropped-1200px-Kabupaten_Wonosobo-267x300.png 267w, https://dispaperkan.wonosobokab.go.id/wp-content/uploads/2018/10/cropped-1200px-Kabupaten_Wonosobo-768x861.png 768w, https://dispaperkan.wonosobokab.go.id/wp-content/uploads/2018/10/cropped-1200px-Kabupaten_Wonosobo-913x1024.png 913w, https://dispaperkan.wonosobokab.go.id/wp-content/uploads/2018/10/cropped-1200px-Kabupaten_Wonosobo.png 1200w"
                                    sizes="(max-width: 213px) 100vw, 213px" /></a></p>
                        <p>&nbsp;</p>
                        <p style="text-align: center;"><span style="color: #339966;"><strong>DINAS PANGAN PERTANIAN DAN
                                    PERIKANAN KABUPATEN WONOSOBO</strong></span></p>
                        <p style="text-align: center;"><span style="color: #339966;"><strong>PEMERINTAH
                                </strong><strong>KABUPATEN WONOSOBO</strong></span></p>
                        <p style="text-align: center;"><strong> </strong></p>
                        <p style="text-align: center;"><span style="color: #000000;"><strong>PEJABAT PENGELOLA INFORMASI
                                    DAERAH</strong></span></p>
                        <p style="text-align: center;">
                        <p style="text-align: center;"><strong>Jl. Soekarno Hatta No. 3</strong></p>
                        <p style="text-align: center;"><strong>Telp. (0286) 321036 Fax. (0286) 322739</strong></p>
                        <p style="text-align: center;"><strong>Email : <a
                                    href="mailto:dispaperkan@wonosobokab.go.id">dispaperkan@wonosobokab.go.id</a></strong>
                        </p>
                        <p style="text-align: center;"><strong> </strong></p>
                        <p style="text-align: center;"><span style="color: #ff0000;"><strong>JAM LAYANAN
                                    INFORMASI</strong></span></p>
                        <p style="text-align: center;"><span style="color: #ff0000;"><strong>Setiap Hari Kerja (Jam 09.00 –
                                    15 .00 WIB)</strong></span></p>

                    </div>
                    <!-- End License -->

                    <!-- Ownership -->
                    <div id="ownership" class="u-shadow-v19 g-bg-white rounded g-pa-40 g-mb-50">
                        <h3 class="g-color-black mb-3">Struktur PPID Dispaperkan</h3>
                        <div class="d-inline-block g-width-50 g-height-2 g-bg-black mb-3"></div>
                        <p><a href="https://dispaperkan.wonosobokab.go.id/wp-content/uploads/2018/10/Struktur-PPID-1.jpg"><img
                                    class="alignleft wp-image-3366 size-large"
                                    src="https://dispaperkan.wonosobokab.go.id/wp-content/uploads/2018/10/Struktur-PPID-1-1024x724.jpg"
                                    alt="" width="640" height="453"
                                    srcset="https://dispaperkan.wonosobokab.go.id/wp-content/uploads/2018/10/Struktur-PPID-1-1024x724.jpg 1024w, https://dispaperkan.wonosobokab.go.id/wp-content/uploads/2018/10/Struktur-PPID-1-300x212.jpg 300w, https://dispaperkan.wonosobokab.go.id/wp-content/uploads/2018/10/Struktur-PPID-1-768x543.jpg 768w"
                                    sizes="(max-width: 640px) 100vw, 640px" /></a></p>

                    </div>
                    <!-- End Ownership -->

                    <!-- Usage -->
                    <div id="usage" class="u-shadow-v19 g-bg-white rounded g-pa-40 g-mb-50">
                        <h3 class="g-color-black mb-3">Tugas PPID</h3>
                        <div class="d-inline-block g-width-50 g-height-2 g-bg-black mb-3"></div>
                        <p><span style="color: #800080;"><strong>TUGAS PPID</strong></span></p>
                        <p><span style="color: #800080;"><strong>PEJABAT PENGELOLA INFORMASI DAN DOKUMENTASI</strong></span>
                        </p>
                        <p><span style="color: #800080;"><strong>TUGAS DAN TANGGUNG JAWAB</strong></span></p>
                        <p><span style="color: #800080;">(Peraturan Pemerintah No. 61 Tahun 2010)</span></p>
                        <ol>
                            <li><span style="color: #800080;">Penyediaan Informasi;</span></li>
                            <li><span style="color: #800080;">Penyimpanan;</span></li>
                            <li><span style="color: #800080;">Pendokumentasian; dan</span></li>
                            <li><span style="color: #800080;">Pengamanan Informasi;</span></li>
                            <li><span style="color: #800080;">Pelayanan informasi yang cepat, tepat dan sederhana sesuai
                                    dengan aturan yang berlaku;</span></li>
                            <li><span style="color: #800080;">Penetapan Prosedur Operasional Penyebarluasan Informasi
                                    Publik;</span></li>
                            <li><span style="color: #800080;">Pengujian konsekuensi;</span></li>
                            <li><span style="color: #800080;">Pengklasifikasian informasi dan/ atau pengubahannya;</span>
                            </li>
                            <li><span style="color: #800080;">Penetapan informasi yang dikecualikan yang telah habis jangka
                                    waktu pengecualiannya sebagai informasi publik yang dapat diakses;</span></li>
                            <li><span style="color: #800080;">Penetapan pertimbangan tertulis atas setiap kebijakan yang
                                    diambil untuk memenuhi hak setiap orang atas Informasi Publik.</span></li>
                        </ol>
                        <p>&nbsp;</p>
                        <p><span style="color: #800080;"><strong>TUGAS PEJABAT PENGELOLA INFORMASI DAN DOKUMENTASI
                                    PEMBANTU</strong></span></p>
                        <p><span style="color: #800080;">(Peraturan Gubernur Jawa Tengah No. 12 Tahun 2015)</span></p>
                        <ol>
                            <li><span style="color: #800080;">Pengklasifikasian informasi terdiri dari :</span>
                                <ul>
                                    <li><span style="color: #800080;">Informasi yang wajib disediakan dan diumumkan secara
                                            berkala;</span></li>
                                    <li><span style="color: #800080;">Informasi yang wajib diumumkan secara serta
                                            merta;</span></li>
                                    <li><span style="color: #800080;">Informasi yang wajib tersedia setiap saat;</span></li>
                                    <li><span style="color: #800080;">Informasi yang dikcualikan.</span></li>
                                </ul>
                            </li>
                            <li><span style="color: #800080;">Mengkoordinasikan dan mengkonsolidasikan pengumpulan bahan
                                    informasi dan dokumentasi yang ada dilingkungannya;</span></li>
                            <li><span style="color: #800080;">Menyimpan, mendokumentasikan, mnyediakan dan memberi pelayanan
                                    informasi yang ada dilingkungannya kepada publik;</span></li>
                            <li><span style="color: #800080;">Melakukan verifiksai bahan informsai publik yang ada
                                    dilingkungannya.</span></li>
                            <li><span style="color: #800080;">Mlakukan pemutakhiran informasi dan dokumentaaasi yang ada
                                    dilingkuangannya;</span></li>
                            <li><span style="color: #800080;">Menyediakan informasi dan dokumentasi yang ada dilingkungannya
                                    untuk diakses oleh masyarakat;</span></li>
                            <li><span style="color: #800080;">Melakukan inventarisai iniformasi yang dikecualikan untuk
                                    disampaikan kepada PPID Utama;</span></li>
                            <li><span style="color: #800080;">Memberikan laporan tentang pengelolaan informasi yang ada
                                    dilingkungannya kepada PPID Utama secara berkala.</span></li>
                        </ol>
                        <p>&nbsp;</p>
                    </div>
                    <!-- End Usage -->

                    <!-- Refunds -->
                    {{-- <div id="refunds" class="u-shadow-v19 g-bg-white rounded g-pa-40">
                        <h3 class="g-color-black mb-3">Refunds</h3>
                        <div class="d-inline-block g-width-50 g-height-2 g-bg-black mb-3"></div>
                        <p>This is where we sit down, grab a cup of coffee and dial in the details. Understanding the task
                            at hand and ironing out the wrinkles is key. Now that we've aligned the details, it's time to
                            get things mapped out and organized. This part
                            is really crucial in keeping the project in line to completion.</p>
                        <p>Whether through commerce or just an experience to tell your brand's story, the time has come to
                            start using development languages that fit your projects needs. Now that your brand is all
                            dressed up and ready to party, it's time to release
                            it to the world. By the way, let's celebrate already. We get it, you're busy and it's important
                            that someone keeps up with marketing and driving people to your brand. We've got you covered.
                        </p>
                    </div> --}}
                    <!-- End Refunds -->
                </div>
            </div>
        </div>
    </section>
    <!-- End Content -->

    <div id="stickyblock-end"></div>
@stop
