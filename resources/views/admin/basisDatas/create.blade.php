@formField('input', [
    'name' => $titleFormKey ?? 'title',
    'label' => $titleFormKey === 'title' ? twillTrans('twill::lang.modal.title-field') : ucfirst($titleFormKey),
    'translated' => $translateTitle ?? false,
    'required' => true,
    'onChange' => 'formatPermalink'
])

@formField('input', [
    'name' => 'year',
    'label' => 'Tahun',
    'translated' => false,
    'required' => true
])

@formField('select', [
    'name' => 'category_id',
    'label' => 'Category',
    'placeholder' => 'Select an category',
    'options' => $categoryList
])

@if ($permalink ?? true)
    @formField('input', [
        'name' => 'slug',
        'label' => twillTrans('twill::lang.modal.permalink-field'),
        'translated' => true,
        'ref' => 'permalink',
        'prefix' => $permalinkPrefix ?? ''
    ])
@endif
