@extends('twill::layouts.form')

@section('contentFields')
    @formField('wysiwyg', [
    'translated' => false,
    'name' => 'description',
    'label' => 'Description',
    'maxlength' => 100000,
    'editSource' => true,
    'note' => 'You can edit the source.',
    'toolbarOptions' => [
    ['header' => [1, 2, 3, 4, 5, 6, false]],
    'bold',
    'italic',
    'underline',
    'strike',
    ['color'=> [] , 'background'=> [] ],
    ['background'=> [] , 'background'=> [] ],
    "blockquote",
    "code-block",
    ['list' => 'ordered'],
    ['list' => 'bullet'],
    ['indent' => '-1'],
    ['indent' => '+1'],
    ["align" => []],
    ["direction" => "rtl"],
    'link',
    'image',
    'video',
    'clean',
    ],
    ])
@stop
