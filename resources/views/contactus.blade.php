@extends('layout.master')
@section('content')
    <!-- Contact Form -->
    <section class="container g-py-50">
        <div class="row g-mb-20">
            <div class="col-lg-6 g-mb-50">
                <!-- Heading -->
                <h2 class="h1 g-color-black g-font-weight-700 mb-4">Ada yang bisa kami lakukan untuk membantumu?</h2>
                <p class="g-font-size-18 mb-0"></p>
                <!-- End Heading -->
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-5">
                <form>
                    <div class="row">
                        <div class="col-md-12 form-group g-mb-20">
                            <label class="g-color-gray-dark-v2 g-font-size-13">Nama Lengkap</label>
                            <input
                                class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus rounded-3 g-py-13 g-px-15"
                                type="text" placeholder="John">
                        </div>
                    </div>

                    <div class="g-mb-20">
                        <label class="g-color-gray-dark-v2 g-font-size-13">Email</label>
                        <input
                            class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus rounded-3 g-py-13 g-px-15"
                            type="email" placeholder="johndoe@gmail.com">
                    </div>

                    <div class="g-mb-20">
                        <label class="g-color-gray-dark-v2 g-font-size-13">Nomor HP</label>
                        <input
                            class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus rounded-3 g-py-13 g-px-15"
                            type="tel" placeholder="+ (62)8232-2223-3544">
                    </div>

                </form>
            </div>
            <div class="col-md-7">
                <div class="g-mb-40">
                    <label class="g-color-gray-dark-v2 g-font-size-13">Pesanmu</label>
                    <textarea
                        class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus g-resize-none rounded-3 g-py-13 g-px-15"
                        rows="12" placeholder="Hai, Tulis pesanmu disini ..."></textarea>
                </div>

                <div class="text-right">
                    <button
                        class="btn u-btn-primary g-font-weight-600 g-font-size-13 text-uppercase rounded-3 g-py-12 g-px-35"
                        type="submit" role="button">Kirim</button>
                </div>
            </div>
        </div>
    </section>
    <!-- End Contact Form -->
@stop
