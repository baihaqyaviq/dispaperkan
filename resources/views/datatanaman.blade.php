@extends('layout.master')
@section('content')
    <!-- Breadcrumbs -->
    <section class="g-bg-gray-light-v5 g-py-50">
        <div class="container">
            <div class="d-sm-flex text-center">
                <div class="align-self-center">
                    <h2 class="h3 g-font-weight-300 w-100 g-mb-10 g-mb-0--md">DATA TANAMAN PANGAN</h2>
                </div>

                <div class="align-self-center ml-auto">
                    <ul class="u-list-inline">
                        <li class="list-inline-item g-mr-5">
                            <a class="u-link-v5 g-color-main g-color-primary--hover" href="#">BASIS DATA</a>
                            <i class="g-color-gray-light-v2 g-ml-5">/</i>
                        </li>
                        <li class="list-inline-item g-color-primary">
                            <span>DATA TANAMAN PANGAN</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- End Breadcrumbs -->


    <!-- Portfolio Single Item -->
    <section class="container g-py-50">
        <div class="row g-mb-0">
            <div class="col-md-8 g-mb-30">
                <h2 class="h1 g-color-black">Luas Tanam dan Luas Panen Ubi Kayu MT II Tahun 2020 Kabupaten Wonosobo</h2>
                <figure class="highcharts-figure">
                    <div id="container"></div>
                    <p class="highcharts-description">

                    </p>
                </figure>
            </div>

            <div class="col-md-4 g-mb-30">
                <!-- Client -->
                <div class="mb-5">
                    <h3 class="h5 g-color-black mb-3">Client:</h3>
                    <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#">
                        <img class="g-width-25 g-height-25 rounded-circle mr-2 mb-1"
                            src="../../assets/img-temp/100x100/img1.jpg" alt="Image Description">Htmlstream
                    </a>
                </div>
                <!-- End Client -->

                <!-- Designers -->
                <div class="mb-5">
                    <h3 class="h5 g-color-black mb-3">Designers:</h3>
                    <ul class="list-unstyled">
                        <li class="my-3">
                            <img class="g-width-25 g-height-25 rounded-circle mb-1 mr-2"
                                src="../../assets/img-temp/100x100/img7.jpg" alt="Image Description">
                            <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#">Alex Teseira</a>
                        </li>
                    </ul>
                </div>
                <!-- End Designers -->

                <!-- Tags -->
                <div class="g-mb-30">
                    <h3 class="h5 g-color-black mb-3">Tags:</h3>
                    <ul class="u-list-inline mb-0">
                        <li class="list-inline-item g-mb-10">
                            <a class="u-tags-v1 g-color-main g-brd-around g-brd-gray-light-v3 g-bg-gray-dark-v2--hover g-brd-gray-dark-v2--hover g-color-white--hover g-rounded-50 g-py-4 g-px-15"
                                href="#">Design</a>
                        </li>
                        <li class="list-inline-item g-mb-10">
                            <a class="u-tags-v1 g-color-main g-brd-around g-brd-gray-light-v3 g-bg-gray-dark-v2--hover g-brd-gray-dark-v2--hover g-color-white--hover g-rounded-50 g-py-4 g-px-15"
                                href="#">Art</a>
                        </li>
                        <li class="list-inline-item g-mb-10">
                            <a class="u-tags-v1 g-color-main g-brd-around g-brd-gray-light-v3 g-bg-gray-dark-v2--hover g-brd-gray-dark-v2--hover g-color-white--hover g-rounded-50 g-py-4 g-px-15"
                                href="#">Graphic</a>
                        </li>
                    </ul>
                </div>
                <!-- End Tags -->

                <!-- Share -->
                <div class="mb-3">
                    <h3 class="h5 g-color-black mb-3">Share:</h3>
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item mr-1 mb-1">
                            <a class="btn u-btn-outline-facebook g-rounded-25" href="#">
                                <i class="mr-1 fa fa-facebook"></i>
                                Facebook
                            </a>
                        </li>
                        <li class="list-inline-item mx-1 mb-1">
                            <a class="btn u-btn-outline-twitter g-rounded-25" href="#">
                                <i class="mr-1 fa fa-twitter"></i>
                                Twitter
                            </a>
                        </li>
                        <li class="list-inline-item mx-1 mb-1">
                            <a class="btn u-btn-outline-dribbble g-rounded-25" href="#">
                                <i class="mr-1 fa fa-dribbble"></i>
                                Dribbble
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- End Share -->
            </div>
        </div>
    </section>
    <!-- End Portfolio Single Item -->


    <!-- Portfolio Single Item -->
    <section class="container g-py-50">
        <div class="row g-mb-0">
            <div class="col-md-8 g-mb-30">
                <h2 class="h1 g-color-black">Luas Tanam dan Luas Panen Padi MT II Tahun 2020 Kabupaten Wonosobo</h2>

                <!--Basic Table-->
                <div id="table">
                    <bootstrap-table :columns="columns" :data="data" :options="options"></bootstrap-table>
                </div>
                <!--End Basic Table-->

            </div>

            <div class="col-md-4 g-mb-30">
                <!-- Client -->
                <div class="mb-5">
                    <h3 class="h5 g-color-black mb-3">Client:</h3>
                    <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#">
                        <img class="g-width-25 g-height-25 rounded-circle mr-2 mb-1"
                            src="../../assets/img-temp/100x100/img1.jpg" alt="Image Description">Htmlstream
                    </a>
                </div>
                <!-- End Client -->

                <!-- Designers -->
                <div class="mb-5">
                    <h3 class="h5 g-color-black mb-3">Designers:</h3>
                    <ul class="list-unstyled">
                        <li class="my-3">
                            <img class="g-width-25 g-height-25 rounded-circle mb-1 mr-2"
                                src="../../assets/img-temp/100x100/img7.jpg" alt="Image Description">
                            <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#">Alex Teseira</a>
                        </li>
                    </ul>
                </div>
                <!-- End Designers -->

                <!-- Tags -->
                <div class="g-mb-30">
                    <h3 class="h5 g-color-black mb-3">Tags:</h3>
                    <ul class="u-list-inline mb-0">
                        <li class="list-inline-item g-mb-10">
                            <a class="u-tags-v1 g-color-main g-brd-around g-brd-gray-light-v3 g-bg-gray-dark-v2--hover g-brd-gray-dark-v2--hover g-color-white--hover g-rounded-50 g-py-4 g-px-15"
                                href="#">Design</a>
                        </li>
                        <li class="list-inline-item g-mb-10">
                            <a class="u-tags-v1 g-color-main g-brd-around g-brd-gray-light-v3 g-bg-gray-dark-v2--hover g-brd-gray-dark-v2--hover g-color-white--hover g-rounded-50 g-py-4 g-px-15"
                                href="#">Art</a>
                        </li>
                        <li class="list-inline-item g-mb-10">
                            <a class="u-tags-v1 g-color-main g-brd-around g-brd-gray-light-v3 g-bg-gray-dark-v2--hover g-brd-gray-dark-v2--hover g-color-white--hover g-rounded-50 g-py-4 g-px-15"
                                href="#">Graphic</a>
                        </li>
                    </ul>
                </div>
                <!-- End Tags -->

                <!-- Share -->
                <div class="mb-3">
                    <h3 class="h5 g-color-black mb-3">Share:</h3>
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item mr-1 mb-1">
                            <a class="btn u-btn-outline-facebook g-rounded-25" href="#">
                                <i class="mr-1 fa fa-facebook"></i>
                                Facebook
                            </a>
                        </li>
                        <li class="list-inline-item mx-1 mb-1">
                            <a class="btn u-btn-outline-twitter g-rounded-25" href="#">
                                <i class="mr-1 fa fa-twitter"></i>
                                Twitter
                            </a>
                        </li>
                        <li class="list-inline-item mx-1 mb-1">
                            <a class="btn u-btn-outline-dribbble g-rounded-25" href="#">
                                <i class="mr-1 fa fa-dribbble"></i>
                                Dribbble
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- End Share -->
            </div>
        </div>
    </section>
    <!-- End Portfolio Single Item -->

@stop

@push('styles')
    <link href="https://unpkg.com/bootstrap-table@1.18.3/dist/bootstrap-table.min.css" rel="stylesheet">
@endpush
@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.3/dist/bootstrap-table.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.2/dist/bootstrap-table-vue.min.js"></script>
    <script>
        $(function() {
            new Vue({
                el: '#table',
                components: {
                    'BootstrapTable': BootstrapTable
                },
                data: {
                    columns: [{
                            field: 'state',
                            checkbox: true
                        },
                        {
                            title: 'Item ID',
                            field: 'id'
                        },
                        {
                            field: 'name',
                            title: 'Item Name'
                        },
                        {
                            field: 'price',
                            title: 'Item Price'
                        },
                        {
                            field: 'action',
                            title: 'Actions',
                            align: 'center',
                            formatter: function() {
                                return '<a href="javascript:" class="like"><i class="fa fa-star"></i></a>'
                            },
                            events: {
                                'click .like': function(e, value, row) {
                                    alert(JSON.stringify(row))
                                }
                            }
                        }
                    ],
                    data: [{
                            id: 0,
                            name: 'Item 0',
                            price: '$0'
                        },
                        {
                            id: 1,
                            name: 'Item 1',
                            price: '$1'
                        },
                        {
                            id: 2,
                            name: 'Item 2',
                            price: '$2'
                        },
                        {
                            id: 3,
                            name: 'Item 3',
                            price: '$3'
                        },
                        {
                            id: 4,
                            name: 'Item 4',
                            price: '$4'
                        },
                        {
                            id: 5,
                            name: 'Item 5',
                            price: '$5'
                        }
                    ],
                    options: {
                        search: true,
                        showColumns: true
                    }
                }
            })
        })

    </script>
    <script>
        Highcharts.chart('container', {
            chart: {
                type: 'line'
            },
            title: {
                text: 'Data Tanaman Pangan'
            },
            subtitle: {
                text: 'Source: WorldClimate.com'
            },
            xAxis: {
                categories: ['May', 'Jun', 'Jul', 'Aug', 'May-Aug']
            },
            yAxis: {
                title: {
                    text: 'Luas Tanaman (HA)'
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: [{
                name: 'Wadaslintang',
                data: [100, 15, 19, 0, 134]
            }, {
                name: 'Kepil',
                data: [89, 74, 81, 77, 320]
            }, {
                name: 'Sapuran',
                data: [27, 25, 18, 19, 89]
            }, {
                name: 'Kalibawang',
                data: [15, 125, 37, 56, 233]
            }, {
                name: 'Kaliwiro',
                data: [0, 0, 0, 0, 0]
            }, {
                name: 'Leksono',
                data: [0, 69, 0, 0, 69]
            }, {
                name: 'Sukoharjo',
                data: [2, 2, 1, 2, 7]
            }, {
                name: 'Selomerto',
                data: [8, 2, 4, 4, 18]
            }, {
                name: 'Kalikajar',
                data: [22, 8, 0, 9, 39]
            }, {
                name: 'Kertek',
                data: [3, 2, 1, 1, 7]
            }, {
                name: 'Wonosobo',
                data: [0, 3, 3, 3, 9]
            }, {
                name: 'Watumalang',
                data: [16, 16, 11, 9, 52]
            }, {
                name: 'Mojotengah',
                data: [32, 82, 54, 76, 243]
            }, {
                name: 'Garung',
                data: [38, 88, 72, 64, 262]
            }, {
                name: 'Kejajar',
                data: [0, 0, 0, 0, 0]
            }, ]
        });

    </script>

    <!-- JS Implementing Plugins -->
    <script src="{{ asset('assets/vendor/jquery.peity.min.js') }}"></script>

    <!-- JS Unify -->
    <script src="{{ asset('assets/js/components/hs.chart.js') }}"></script>

    <!-- JS Plugins Init. -->
    <script>
        $(document).ready(function() {
            // initialization of peity charts
            $.HSCore.components.HSChart.peity.init($('.js-peity-chart'));
        });

    </script>
@endpush
