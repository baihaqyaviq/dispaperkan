@extends('layout.master')
@section('content')
    <!-- Breadcrumbs -->
    <section class="g-bg-gray-light-v5 g-py-50">
        <div class="container">
            <div class="d-sm-flex text-center">
                <div class="align-self-center">
                    <h2 class="h3 g-font-weight-300 w-100 g-mb-10 g-mb-0--md">Visi</h2>
                </div>

                <div class="align-self-center ml-auto">
                    <ul class="u-list-inline">
                        <li class="list-inline-item g-mr-5">
                            <a class="u-link-v5 g-color-main g-color-primary--hover" href="#">Tentang Kami</a>
                            <i class="g-color-gray-light-v2 g-ml-5">/</i>
                        </li>
                        <li class="list-inline-item g-color-primary">
                            <span>Visi</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- End Breadcrumbs -->


    <!-- Portfolio Single Item -->
    <section class="container g-py-50">

        <div class="row g-mb-70">
            <div class="col-md-8 g-mb-30">
                <div class="mb-5">
                    <p>
                        Visi adalah rumusan umum mengenai keadaan yang diinginkan pada akhir periode perencanaan. Visi
                        adalah gambaran yang menantang tentang keadaan masa depan yang diinginkan oleh Instansi Pemerintah.
                        Mengacu kepada kondisi yang diharapkan tersebut, maka dirumuskan visi Dinas Peternakan dan Perikanan
                        Kabupaten Wonosobo , merujuk pada visi Kabupaten Wonosobo: </p>

                    </p><b>"TERWUJUDNYA WONOSOBO BERSATU UNTUK MAJU, MANDIRI DAN
                        SEJAHTERA UNTUK SEMUA"</b></p>

                    <p><b>Maju :</b> Adalah semangat dan kerangka berfikir serta bertindak oleh setiap
                        pribadi dan lembaga penyelenggara pemerintahan daerah dalam
                        mengatur, melayani, membangun dan memberdayakan masyarakat.
                        Bersatu juga menjadi semangat dan kerangka berperilaku masyarakat
                        dalam menyampaiakan “tuntutan” maupun “dukungan” kepada
                        penyelenggara pemerintahan daerah, dalam rangka meningkatkan
                        kesejahteraannya. Selain itu, bersatu adalah motivasi masyarakat sipil
                        dalam memfasilitasi hubungan masyarakat dan pemerintah daerah
                        serta mengontrol pemerintah daerah dalam menjalankan tugas,
                        fungsi, hak, wewenang dan kewajibanya. Dengan bersatunya birokrat,
                        politisi, masyarakat sipil dan masyarakat akan mempercepat
                        terwujudnya tata kelola pemerintahan yang baik dan meningkatkan
                        rasa kemanusiaan, toleransi dan keharmonisan untuk hidup secara
                        berdamupingan, sehingga terpelihara situasi ketentraman dan
                        ketertiban umum di seluruh wilayah Kabupaten Wonosobo;</p>

                    <p><b>Mandiri :</b> adalah suatu kondisi yang mencirikan kemampuan daerah untuk
                        berdiri dengan kekuatan dan kemampuan sendiri sesuai dengan
                        semangat otonomi daerah. Ketergantungan bantuan dari Pemerintah
                        dan Provinsi secara bertahap harus dikurangi. Oleh karena itu, semua
                        potensi keunggulan daerah, yang dalam struktur pembagian urusan
                        pemerintahan dikenal dengan urusan pemerintahan pilihan akan
                        dikelola lebih optimal, sehingga lebih produktif dan kontributif dalam
                        mengurangi ketergantungan daerah. Untuk itu, produksi dan
                        produktivitas daerah perlu terus dioptimalkan peningkatanya,
                        sehingga Wonosobo akan mampu meningkatkan daya saing daerah
                        dalam kancah percaturan regional, nasional bahkan global;</p>

                    <p><b>Sejahtera untuk Semua :</b> tujuan akhir dari penyelenggaran
                        pemerintahan daerah dimanapun entitasnya adalah untuk
                        kesejahteraan masyarakat. Namun demikian peningkatan
                        kesejahteraan tidak boleh hanya dinikmati oleh sekelompok atau
                        golongan masyarakat tertentu tetapi harus bisa dinikmati oleh
                        seluruh masyarakat Wonosobo. Oleh karenanya, percepatan
                        penurunan angka kemiskinan akan terus dioptimalkan
                        pelaksanaannya. Demikian halnya dengan ketimpangan pendapatan
                        antar golongan penduduk dan ketimpangan pertumbuhan antar
                        wilayah akan terus diminimalisasikan, sehingga peningkatan
                        kesejahteraan yang dicapai oleh pemerintahan 2016 – 2021 akan
                        dirasakan oleh semua masyarakat disemua wilayah Wonosobo.
                    <p>
                </div>
            </div>

            <div class="col-md-4 g-mb-30">
                <!-- Client -->
                <div class="mb-5">
                    <h3 class="h5 g-color-black mb-3">Published:</h3>28 December 2021
                </div>
                <!-- End Client -->

                <!-- Designers -->
                <div class="mb-5">
                    <h3 class="h5 g-color-black mb-3">Admin:</h3>
                    <ul class="list-unstyled">
                        <li class="my-3">
                            <img class="g-width-25 g-height-25 rounded-circle mb-1 mr-2"
                                src="../../assets/img-temp/100x100/img7.jpg" alt="Image Description">
                            <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#">Alex Teseira</a>
                        </li>
                    </ul>
                </div>
                <!-- End Designers -->

                <!-- Tags -->
                <div class="g-mb-30">
                    <h3 class="h5 g-color-black mb-3">Tags:</h3>
                    <ul class="u-list-inline mb-0">
                        <li class="list-inline-item g-mb-10">
                            <a class="u-tags-v1 g-color-main g-brd-around g-brd-gray-light-v3 g-bg-gray-dark-v2--hover g-brd-gray-dark-v2--hover g-color-white--hover g-rounded-50 g-py-4 g-px-15"
                                href="#">Design</a>
                        </li>
                        <li class="list-inline-item g-mb-10">
                            <a class="u-tags-v1 g-color-main g-brd-around g-brd-gray-light-v3 g-bg-gray-dark-v2--hover g-brd-gray-dark-v2--hover g-color-white--hover g-rounded-50 g-py-4 g-px-15"
                                href="#">Art</a>
                        </li>
                        <li class="list-inline-item g-mb-10">
                            <a class="u-tags-v1 g-color-main g-brd-around g-brd-gray-light-v3 g-bg-gray-dark-v2--hover g-brd-gray-dark-v2--hover g-color-white--hover g-rounded-50 g-py-4 g-px-15"
                                href="#">Graphic</a>
                        </li>
                    </ul>
                </div>
                <!-- End Tags -->

                <!-- Share -->
                <div class="mb-3">
                    <h3 class="h5 g-color-black mb-3">Share:</h3>
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item mr-1 mb-1">
                            <a class="btn u-btn-outline-facebook g-rounded-25" href="#">
                                <i class="mr-1 fa fa-facebook"></i>
                                Facebook
                            </a>
                        </li>
                        <li class="list-inline-item mx-1 mb-1">
                            <a class="btn u-btn-outline-twitter g-rounded-25" href="#">
                                <i class="mr-1 fa fa-twitter"></i>
                                Twitter
                            </a>
                        </li>
                        <li class="list-inline-item mx-1 mb-1">
                            <a class="btn u-btn-outline-dribbble g-rounded-25" href="#">
                                <i class="mr-1 fa fa-dribbble"></i>
                                Dribbble
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- End Share -->
            </div>
        </div>
    </section>
    <!-- End Portfolio Single Item -->

@stop
