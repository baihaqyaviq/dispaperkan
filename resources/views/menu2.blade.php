@extends('layout.master')
@section('content')
    <!-- Breadcrumbs -->
    <section class="g-bg-gray-light-v5 g-py-50">
        <div class="container">
            <div class="d-sm-flex text-center">
                <div class="align-self-center">
                    <h2 class="h3 g-font-weight-300 w-100 g-mb-10 g-mb-0--md">Judul</h2>
                </div>

                <div class="align-self-center ml-auto">
                    <ul class="u-list-inline">
                        <li class="list-inline-item g-mr-5">
                            <a class="u-link-v5 g-color-main g-color-primary--hover" href="#">Tentang Kami</a>
                            <i class="g-color-gray-light-v2 g-ml-5">/</i>
                        </li>
                        <li class="list-inline-item g-color-primary">
                            <span>Judul</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- End Breadcrumbs -->


    <!-- Portfolio Single Item -->
    <section class="container g-py-100">

        <div class="row g-mb-70">
            <div class="col-md-8 g-mb-30">
                <div class="mb-5">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                        industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type
                        and scrambled it to make a type specimen book. It has survived not only five centuries, but also the
                        leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s
                        with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop
                        publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </p>
                </div>
            </div>

            <div class="col-md-4 g-mb-30">

                <!-- Designers -->
                <div class="mb-5">
                    <h3 class="h5 g-color-black mb-3">Authors:</h3>
                    <ul class="list-unstyled">
                        <li class="my-3">
                            <img class="g-width-25 g-height-25 rounded-circle mb-1 mr-2"
                                src="../../assets/img-temp/100x100/img7.jpg" alt="Image Description">
                            <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#">Alex Teseira</a>
                        </li>
                    </ul>
                </div>
                <!-- End Designers -->

                <!-- Share -->
                <div class="mb-3">
                    <h3 class="h5 g-color-black mb-3">Share:</h3>
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item mr-1 mb-1">
                            <a class="btn u-btn-outline-facebook g-rounded-25" href="#">
                                <i class="mr-1 fa fa-facebook"></i>
                                Facebook
                            </a>
                        </li>
                        <li class="list-inline-item mx-1 mb-1">
                            <a class="btn u-btn-outline-twitter g-rounded-25" href="#">
                                <i class="mr-1 fa fa-twitter"></i>
                                Twitter
                            </a>
                        </li>
                        <li class="list-inline-item mx-1 mb-1">
                            <a class="btn u-btn-outline-dribbble g-rounded-25" href="#">
                                <i class="mr-1 fa fa-dribbble"></i>
                                Dribbble
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- End Share -->
            </div>
        </div>
    </section>
    <!-- End Portfolio Single Item -->

@stop
