@extends('layout.master')
@section('content')
    <!-- Breadcrumbs -->
    <section class="g-bg-gray-light-v5 g-py-50">
        <div class="container">
            <div class="d-sm-flex text-center">
                <div class="align-self-center">
                    <h2 class="h3 g-font-weight-300 w-100 g-mb-10 g-mb-0--md">KELEMBAGAAN PETANI</h2>
                </div>

                <div class="align-self-center ml-auto">
                    <ul class="u-list-inline">
                        <li class="list-inline-item g-mr-5">
                            <a class="u-link-v5 g-color-main g-color-primary--hover" href="#">BASIS DATA</a>
                            <i class="g-color-gray-light-v2 g-ml-5">/</i>
                        </li>
                        <li class="list-inline-item g-color-primary">
                            <span>KELEMBAGAAN PETANI</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- End Breadcrumbs -->

    <!-- Portfolio Single Item -->
    <section class="container g-py-50">
        <div class="row g-mb-0">
            <div class="col-md-8 g-mb-30">
                <h2 class="h1 g-color-black">Rekap Kelembagaan Tani Tahun 2018</h2>
                <!--Basic Table-->
                <div class="table-responsive">
                    <table class="table table-bordered u-table--v2">
                        <thead class="text-uppercase g-letter-spacing-1">
                            <tr>
                                <th class="g-font-weight-300 g-color-black">Users</th>
                                <th class="g-font-weight-300 g-color-black g-min-width-200">About Folks</th>
                                <th class="g-font-weight-300 g-color-black">Stats</th>
                                <th class="g-font-weight-300 g-color-black">Contacts</th>
                                <th class="g-font-weight-300 g-color-black text-nowrap">Work Rate</th>
                                <th class="g-font-weight-300 g-color-black">Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td class="align-middle text-nowrap">
                                    <div class="media">
                                        <img class="d-flex g-width-40 g-height-40 rounded-circle g-mr-10"
                                            src="../../assets/img-temp/100x100/img7.jpg" alt="Image Description">
                                        <div class="media-body align-self-center">
                                            <h5 class="h6 align-self-center g-font-weight-600 g-color-purple mb-0">John
                                                Doe</h5>
                                            <span class="g-font-size-12">Marketing</span>
                                        </div>
                                    </div>
                                </td>
                                <td class="align-middle">Nulla ipsum dolor sit amet, consectetur adipiscing elitut
                                    eleifend nisl.</td>
                                <td class="align-middle text-center">
                                    <span class="js-peity-chart" data-peity-type="bar"
                                        data-fill="['rgba(99, 99, 255, .3)', 'rgba(255, 99, 132, .8)', 'rgba(54, 162, 235, .6)']"
                                        data-max="10">5,3,9,6,5,9,7,3,5,2
                                    </span>
                                </td>
                                <td class="align-middle text-nowrap">
                                    <span class="d-block g-mb-5">
                                        <i
                                            class="icon-phone g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i>
                                        +1 4768 97655
                                    </span>
                                    <span class="d-block">
                                        <i
                                            class="icon-envelope g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i>
                                        contact@lenovo.com
                                    </span>
                                </td>
                                <td class="align-middle text-center">
                                    <span class="u-label g-bg-purple g-rounded-20 g-px-10">$25 / hr</span>
                                </td>
                                <td class="align-middle text-nowrap text-center">
                                    <a class="g-color-gray-dark-v5 g-text-underline--none--hover g-pa-5" href="#"
                                        data-toggle="tooltip" data-placement="top" data-original-title="Edit">
                                        <i class="icon-pencil g-font-size-18 g-mr-7"></i>
                                    </a>
                                    <a class="g-color-gray-dark-v5 g-text-underline--none--hover g-pa-5" href="#"
                                        data-toggle="tooltip" data-placement="top" data-original-title="Delete">
                                        <i class="icon-trash g-font-size-18 g-mr-7"></i>
                                    </a>
                                    <a class="g-color-gray-dark-v5 g-text-underline--none--hover g-pa-5" href="#"
                                        data-toggle="tooltip" data-placement="top" data-original-title="Print">
                                        <i class="icon-printer g-font-size-18 g-mr-7"></i>
                                    </a>
                                </td>
                            </tr>

                            <tr>
                                <td class="align-middle text-nowrap">
                                    <div class="media">
                                        <img class="d-flex g-width-40 g-height-40 rounded-circle g-mr-10"
                                            src="../../assets/img-temp/100x100/img5.jpg" alt="Image Description">
                                        <div class="media-body align-self-center">
                                            <h5 class="h6 align-self-center g-font-weight-600 g-color-teal mb-0">Alberta
                                                Watson</h5>
                                            <span class="g-font-size-12">UX Design</span>
                                        </div>
                                    </div>
                                </td>
                                <td class="align-middle">In consectetur adipiscing hac habitasse platea dictumst,
                                    curabitur hendrerit.</td>
                                <td class="align-middle text-center">
                                    <span class="js-peity-chart" data-peity-type="donut"
                                        data-fill="['rgba(99, 99, 255, .5)', 'rgba(255, 99, 132, .5)', 'rgba(54, 162, 235, .5)']"
                                        data-max="10">3/5
                                    </span>
                                </td>
                                <td class="align-middle text-nowrap">
                                    <span class="d-block g-mb-5">
                                        <i
                                            class="icon-phone g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i>
                                        +44 7689 7655
                                    </span>
                                    <span class="d-block">
                                        <i
                                            class="icon-envelope g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i>
                                        users@samsung.com
                                    </span>
                                </td>
                                <td class="align-middle text-center">
                                    <span class="u-label g-bg-teal g-rounded-20 g-px-10">$32 / hr</span>
                                </td>
                                <td class="align-middle text-nowrap text-center">
                                    <a class="g-color-gray-dark-v5 g-text-underline--none--hover g-pa-5" href="#"
                                        data-toggle="tooltip" data-placement="top" data-original-title="Edit">
                                        <i class="icon-pencil g-font-size-18 g-mr-7"></i>
                                    </a>
                                    <a class="g-color-gray-dark-v5 g-text-underline--none--hover g-pa-5" href="#"
                                        data-toggle="tooltip" data-placement="top" data-original-title="Delete">
                                        <i class="icon-trash g-font-size-18 g-mr-7"></i>
                                    </a>
                                    <a class="g-color-gray-dark-v5 g-text-underline--none--hover g-pa-5" href="#"
                                        data-toggle="tooltip" data-placement="top" data-original-title="Print">
                                        <i class="icon-printer g-font-size-18 g-mr-7"></i>
                                    </a>
                                </td>
                            </tr>

                            <tr>
                                <td class="align-middle text-nowrap">
                                    <div class="media">
                                        <img class="d-flex g-width-40 g-height-40 rounded-circle g-mr-10"
                                            src="../../assets/img-temp/100x100/img17.jpg" alt="Image Description">
                                        <div class="media-body align-self-center">
                                            <h5 class="h6 align-self-center g-font-weight-600 g-color-pink mb-0">David
                                                Lee</h5>
                                            <span class="g-font-size-12">Angular JS</span>
                                        </div>
                                    </div>
                                </td>
                                <td class="align-middle">To a general advertiser outdoor advertising is worthy of
                                    consideration..</td>
                                <td class="align-middle text-center">
                                    <span class="js-peity-chart" data-peity-type="line" data-fill="['rgba(99,99, 255, .1)']"
                                        data-stroke="rgba(99,99, 255, .5)" data-max="10">5,3,9,6,5,9,7,3,5,2
                                    </span>
                                </td>
                                <td class="align-middle text-nowrap">
                                    <span class="d-block g-mb-5">
                                        <i
                                            class="icon-phone g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i>
                                        +1 0739 3644
                                    </span>
                                    <span class="d-block">
                                        <i
                                            class="icon-envelope g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i>
                                        clients@sony.com
                                    </span>
                                </td>
                                <td class="align-middle text-center">
                                    <span class="u-label g-bg-pink g-rounded-20 g-px-10">$28 / hr</span>
                                </td>
                                <td class="align-middle text-nowrap text-center">
                                    <a class="g-color-gray-dark-v5 g-text-underline--none--hover g-pa-5" href="#"
                                        data-toggle="tooltip" data-placement="top" data-original-title="Edit">
                                        <i class="icon-pencil g-font-size-18 g-mr-7"></i>
                                    </a>
                                    <a class="g-color-gray-dark-v5 g-text-underline--none--hover g-pa-5" href="#"
                                        data-toggle="tooltip" data-placement="top" data-original-title="Delete">
                                        <i class="icon-trash g-font-size-18 g-mr-7"></i>
                                    </a>
                                    <a class="g-color-gray-dark-v5 g-text-underline--none--hover g-pa-5" href="#"
                                        data-toggle="tooltip" data-placement="top" data-original-title="Print">
                                        <i class="icon-printer g-font-size-18 g-mr-7"></i>
                                    </a>
                                </td>
                            </tr>

                            <tr>
                                <td class="align-middle text-nowrap">
                                    <div class="media">
                                        <img class="d-flex g-width-40 g-height-40 rounded-circle g-mr-10"
                                            src="../../assets/img-temp/100x100/img14.jpg" alt="Image Description">
                                        <div class="media-body align-self-center">
                                            <h5 class="h6 align-self-center g-font-weight-600 g-color-cyan mb-0">James
                                                Coolman</h5>
                                            <span class="g-font-size-12">Ruby on Rails</span>
                                        </div>
                                    </div>
                                </td>
                                <td>Create a list with all possible keywords that fit to your product, service or
                                    business..</td>
                                <td class="align-middle text-center">
                                    <span class="js-peity-chart" data-peity-type="pie"
                                        data-fill="['rgba(99, 99, 255, .5)', 'rgba(255, 99, 132, .5)', 'rgba(54, 162, 235, .6)']"
                                        data-max="10">2/5
                                    </span>
                                </td>
                                <td class="align-middle text-nowrap">
                                    <span class="d-block g-mb-5">
                                        <i
                                            class="icon-phone g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i>
                                        +1 6589-96451
                                    </span>
                                    <span class="d-block">
                                        <i
                                            class="icon-envelope g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i>
                                        mail@appple.com
                                    </span>
                                </td>
                                <td class="align-middle text-center">
                                    <span class="u-label g-bg-cyan g-rounded-20 g-px-10">$35 / hr</span>
                                </td>
                                <td class="align-middle text-nowrap text-center">
                                    <a class="g-color-gray-dark-v5 g-text-underline--none--hover g-pa-5" href="#"
                                        data-toggle="tooltip" data-placement="top" data-original-title="Edit">
                                        <i class="icon-pencil g-font-size-18 g-mr-7"></i>
                                    </a>
                                    <a class="g-color-gray-dark-v5 g-text-underline--none--hover g-pa-5" href="#"
                                        data-toggle="tooltip" data-placement="top" data-original-title="Delete">
                                        <i class="icon-trash g-font-size-18 g-mr-7"></i>
                                    </a>
                                    <a class="g-color-gray-dark-v5 g-text-underline--none--hover g-pa-5" href="#"
                                        data-toggle="tooltip" data-placement="top" data-original-title="Print">
                                        <i class="icon-printer g-font-size-18 g-mr-7"></i>
                                    </a>
                                </td>
                            </tr>

                            <tr>
                                <td class="align-middle text-nowrap">
                                    <div class="media">
                                        <img class="d-flex g-width-40 g-height-40 rounded-circle g-mr-10"
                                            src="../../assets/img-temp/100x100/img4.jpg" alt="Image Description">
                                        <div class="media-body align-self-center">
                                            <h5 class="h6 align-self-center g-font-weight-600 g-color-green mb-0">Kate
                                                Oliver</h5>
                                            <span class="g-font-size-12">Marketing</span>
                                        </div>
                                    </div>
                                </td>
                                <td class="align-middle">Nulla ipsum dolor sit amet, consectetur adipiscing elitut
                                    eleifend nisl.</td>
                                <td class="align-middle text-center">
                                    <span class="js-peity-chart" data-peity-type="bar"
                                        data-fill="['rgba(99, 99, 255, .3)', 'rgba(255, 99, 132, .8)', 'rgba(54, 162, 235, .6)']"
                                        data-max="10">5,3,9,6,5,9,7,3,5,2
                                    </span>
                                </td>
                                <td class="align-middle text-nowrap">
                                    <span class="d-block g-mb-5">
                                        <i
                                            class="icon-phone g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i>
                                        +1 4768 97655
                                    </span>
                                    <span class="d-block">
                                        <i
                                            class="icon-envelope g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i>
                                        contact@lenovo.com
                                    </span>
                                </td>
                                <td class="align-middle text-center">
                                    <span class="u-label g-bg-green g-rounded-20 g-px-10">$27 / hr</span>
                                </td>
                                <td class="align-middle text-nowrap text-center">
                                    <a class="g-color-gray-dark-v5 g-text-underline--none--hover g-pa-5" href="#"
                                        data-toggle="tooltip" data-placement="top" data-original-title="Edit">
                                        <i class="icon-pencil g-font-size-18 g-mr-7"></i>
                                    </a>
                                    <a class="g-color-gray-dark-v5 g-text-underline--none--hover g-pa-5" href="#"
                                        data-toggle="tooltip" data-placement="top" data-original-title="Delete">
                                        <i class="icon-trash g-font-size-18 g-mr-7"></i>
                                    </a>
                                    <a class="g-color-gray-dark-v5 g-text-underline--none--hover g-pa-5" href="#"
                                        data-toggle="tooltip" data-placement="top" data-original-title="Print">
                                        <i class="icon-printer g-font-size-18 g-mr-7"></i>
                                    </a>
                                </td>
                            </tr>

                            <tr>
                                <td class="align-middle text-nowrap">
                                    <div class="media">
                                        <img class="d-flex g-width-40 g-height-40 rounded-circle g-mr-10"
                                            src="../../assets/img-temp/100x100/img6.jpg" alt="Image Description">
                                        <div class="media-body align-self-center">
                                            <h5 class="h6 align-self-center g-font-weight-600 g-color-lightred mb-0">
                                                Julia Bray</h5>
                                            <span class="g-font-size-12">Management</span>
                                        </div>
                                    </div>
                                </td>
                                <td class="align-middle">In consectetur adipiscing hac habitasse platea dictumst,
                                    curabitur hendrerit.</td>
                                <td class="align-middle text-center">
                                    <span class="js-peity-chart" data-peity-type="donut"
                                        data-fill="['rgba(99, 99, 255, .5)', 'rgba(255, 99, 132, .5)', 'rgba(54, 162, 235, .5)']"
                                        data-max="10">3/9
                                    </span>
                                </td>
                                <td class="align-middle text-nowrap">
                                    <span class="d-block g-mb-5">
                                        <i
                                            class="icon-phone g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i>
                                        +44 7689 7655
                                    </span>
                                    <span class="d-block">
                                        <i
                                            class="icon-envelope g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i>
                                        users@samsung.com
                                    </span>
                                </td>
                                <td class="align-middle text-center">
                                    <span class="u-label g-bg-lightred g-rounded-20 g-px-10">$32 / hr</span>
                                </td>
                                <td class="align-middle text-nowrap text-center">
                                    <a class="g-color-gray-dark-v5 g-text-underline--none--hover g-pa-5" href="#"
                                        data-toggle="tooltip" data-placement="top" data-original-title="Edit">
                                        <i class="icon-pencil g-font-size-18 g-mr-7"></i>
                                    </a>
                                    <a class="g-color-gray-dark-v5 g-text-underline--none--hover g-pa-5" href="#"
                                        data-toggle="tooltip" data-placement="top" data-original-title="Delete">
                                        <i class="icon-trash g-font-size-18 g-mr-7"></i>
                                    </a>
                                    <a class="g-color-gray-dark-v5 g-text-underline--none--hover g-pa-5" href="#"
                                        data-toggle="tooltip" data-placement="top" data-original-title="Print">
                                        <i class="icon-printer g-font-size-18 g-mr-7"></i>
                                    </a>
                                </td>
                            </tr>

                            <tr>
                                <td class="align-middle text-nowrap">
                                    <div class="media">
                                        <img class="d-flex g-width-40 g-height-40 rounded-circle g-mr-10"
                                            src="../../assets/img-temp/100x100/img16.jpg" alt="Image Description">
                                        <div class="media-body align-self-center">
                                            <h5 class="h6 align-self-center g-font-weight-600 g-color-darkpurple mb-0">
                                                Amanda Low</h5>
                                            <span class="g-font-size-12">UI Design</span>
                                        </div>
                                    </div>
                                </td>
                                <td class="align-middle">To a general advertiser outdoor advertising is worthy of
                                    consideration..</td>
                                <td class="align-middle text-center">
                                    <span class="js-peity-chart" data-peity-type="line" data-fill="['rgba(99,99, 255, .1)']"
                                        data-stroke="rgba(99,99, 255, .5)" data-max="10">9,6,5,9,5,3,7,3,5,9
                                    </span>
                                </td>
                                <td class="align-middle text-nowrap">
                                    <span class="d-block g-mb-5">
                                        <i
                                            class="icon-phone g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i>
                                        +1 0739 3644
                                    </span>
                                    <span class="d-block">
                                        <i
                                            class="icon-envelope g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i>
                                        clients@sony.com
                                    </span>
                                </td>
                                <td class="align-middle text-center">
                                    <span class="u-label g-bg-darkpurple g-rounded-20 g-px-10">$33 / hr</span>
                                </td>
                                <td class="align-middle text-nowrap text-center">
                                    <a class="g-color-gray-dark-v5 g-text-underline--none--hover g-pa-5" href="#"
                                        data-toggle="tooltip" data-placement="top" data-original-title="Edit">
                                        <i class="icon-pencil g-font-size-18 g-mr-7"></i>
                                    </a>
                                    <a class="g-color-gray-dark-v5 g-text-underline--none--hover g-pa-5" href="#"
                                        data-toggle="tooltip" data-placement="top" data-original-title="Delete">
                                        <i class="icon-trash g-font-size-18 g-mr-7"></i>
                                    </a>
                                    <a class="g-color-gray-dark-v5 g-text-underline--none--hover g-pa-5" href="#"
                                        data-toggle="tooltip" data-placement="top" data-original-title="Print">
                                        <i class="icon-printer g-font-size-18 g-mr-7"></i>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!--End Basic Table-->
            </div>

            <div class="col-md-4 g-mb-30">
                <!-- Client -->
                <div class="mb-5">
                    <h3 class="h5 g-color-black mb-3">Client:</h3>
                    <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#">
                        <img class="g-width-25 g-height-25 rounded-circle mr-2 mb-1"
                            src="../../assets/img-temp/100x100/img1.jpg" alt="Image Description">Htmlstream
                    </a>
                </div>
                <!-- End Client -->

                <!-- Designers -->
                <div class="mb-5">
                    <h3 class="h5 g-color-black mb-3">Designers:</h3>
                    <ul class="list-unstyled">
                        <li class="my-3">
                            <img class="g-width-25 g-height-25 rounded-circle mb-1 mr-2"
                                src="../../assets/img-temp/100x100/img7.jpg" alt="Image Description">
                            <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#">Alex Teseira</a>
                        </li>
                    </ul>
                </div>
                <!-- End Designers -->

                <!-- Tags -->
                <div class="g-mb-30">
                    <h3 class="h5 g-color-black mb-3">Tags:</h3>
                    <ul class="u-list-inline mb-0">
                        <li class="list-inline-item g-mb-10">
                            <a class="u-tags-v1 g-color-main g-brd-around g-brd-gray-light-v3 g-bg-gray-dark-v2--hover g-brd-gray-dark-v2--hover g-color-white--hover g-rounded-50 g-py-4 g-px-15"
                                href="#">Design</a>
                        </li>
                        <li class="list-inline-item g-mb-10">
                            <a class="u-tags-v1 g-color-main g-brd-around g-brd-gray-light-v3 g-bg-gray-dark-v2--hover g-brd-gray-dark-v2--hover g-color-white--hover g-rounded-50 g-py-4 g-px-15"
                                href="#">Art</a>
                        </li>
                        <li class="list-inline-item g-mb-10">
                            <a class="u-tags-v1 g-color-main g-brd-around g-brd-gray-light-v3 g-bg-gray-dark-v2--hover g-brd-gray-dark-v2--hover g-color-white--hover g-rounded-50 g-py-4 g-px-15"
                                href="#">Graphic</a>
                        </li>
                    </ul>
                </div>
                <!-- End Tags -->

                <!-- Share -->
                <div class="mb-3">
                    <h3 class="h5 g-color-black mb-3">Share:</h3>
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item mr-1 mb-1">
                            <a class="btn u-btn-outline-facebook g-rounded-25" href="#">
                                <i class="mr-1 fa fa-facebook"></i>
                                Facebook
                            </a>
                        </li>
                        <li class="list-inline-item mx-1 mb-1">
                            <a class="btn u-btn-outline-twitter g-rounded-25" href="#">
                                <i class="mr-1 fa fa-twitter"></i>
                                Twitter
                            </a>
                        </li>
                        <li class="list-inline-item mx-1 mb-1">
                            <a class="btn u-btn-outline-dribbble g-rounded-25" href="#">
                                <i class="mr-1 fa fa-dribbble"></i>
                                Dribbble
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- End Share -->
            </div>
        </div>
    </section>
    <!-- End Portfolio Single Item -->

@endsection


@push('styles')
@endpush

@push('scripts')

    <!-- JS Implementing Plugins -->
    <script src="{{ asset('assets/vendor/jquery.peity.min.js') }}"></script>

    <!-- JS Unify -->
    <script src="{{ asset('assets/js/components/hs.chart.js') }}"></script>

    <!-- JS Plugins Init. -->
    <script>
        $(document).ready(function() {
            // initialization of peity charts
            $.HSCore.components.HSChart.peity.init($('.js-peity-chart'));
        });

    </script>
    <script>
        Highcharts.chart('container', {
            chart: {
                type: 'line'
            },
            title: {
                text: 'KELEMBAGAAN PETANI'
            },
            subtitle: {
                text: 'Source: WorldClimate.com'
            },
            xAxis: {
                categories: ['May', 'Jun', 'Jul', 'Aug', 'May-Aug']
            },
            yAxis: {
                title: {
                    text: 'Luas Tanaman (HA)'
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: [{
                name: 'Wadaslintang',
                data: [100, 15, 19, 0, 134]
            }, {
                name: 'Kepil',
                data: [89, 74, 81, 77, 320]
            }, {
                name: 'Sapuran',
                data: [27, 25, 18, 19, 89]
            }, {
                name: 'Kalibawang',
                data: [15, 125, 37, 56, 233]
            }, {
                name: 'Kaliwiro',
                data: [0, 0, 0, 0, 0]
            }, {
                name: 'Leksono',
                data: [0, 69, 0, 0, 69]
            }, {
                name: 'Sukoharjo',
                data: [2, 2, 1, 2, 7]
            }, {
                name: 'Selomerto',
                data: [8, 2, 4, 4, 18]
            }, {
                name: 'Kalikajar',
                data: [22, 8, 0, 9, 39]
            }, {
                name: 'Kertek',
                data: [3, 2, 1, 1, 7]
            }, {
                name: 'Wonosobo',
                data: [0, 3, 3, 3, 9]
            }, {
                name: 'Watumalang',
                data: [16, 16, 11, 9, 52]
            }, {
                name: 'Mojotengah',
                data: [32, 82, 54, 76, 243]
            }, {
                name: 'Garung',
                data: [38, 88, 72, 64, 262]
            }, {
                name: 'Kejajar',
                data: [0, 0, 0, 0, 0]
            }, ]
        });

    </script>
@endpush
