@extends('layout.master')
@section('content')
    <!-- Promo Block -->
    <section class="g-py-50">
        <div class="container">
            <!-- News Section -->
            <div class="row no-gutters">
                <div class="col-lg-6 g-pr-1--lg g-mb-30 g-mb-2--lg">
                    <!-- Article -->
                    <article class="u-block-hover">
                        <figure class="u-shadow-v25 g-bg-cover g-bg-white-gradient-opacity-v1--after">
                            <img class="g-height-300 img-fluid w-100 u-block-hover__main--zoom-v1"
                                src="https://dispaperkan.wonosobokab.go.id/wp-content/uploads/2021/06/WhatsApp-Image-2021-06-19-at-02.34.17.jpeg"
                                alt="Image Description">
                        </figure>

                        <span
                            class="g-hidden-xs-down u-icon-v3 u-icon-size--sm g-font-size-13 g-bg-white rounded-circle g-pos-abs g-top-30 g-right-30">
                            <i class="icon-camera"></i>
                        </span>

                        <span class="g-hidden-xs-down g-pos-abs g-top-30 g-left-30">
                            <a class="btn btn-xs u-btn-darkpurple text-uppercase rounded-0" href="#">Berita</a>
                        </span>

                        <div class="g-pos-abs g-bottom-30 g-left-30 g-right-30">
                            <small class="g-color-white">
                                <i class="icon-clock g-pos-rel g-top-1 g-mr-2"></i> July 8, 2016
                            </small>

                            <h3 class="h4 g-my-10">
                                <a class="g-color-white g-color-white--hover" href="#">Rapat Koordinasi Kegiatan
                                    Pekarangan Pangan Lestari (P2l) Kabupaten Wonosobo Ta. 2021</a>
                            </h3>

                            <ul class="g-hidden-xs-down u-list-inline g-font-size-12 g-color-white">
                                <li class="list-inline-item">
                                    <i class="icon-eye g-pos-rel g-top-1 g-mr-2"></i> 264
                                </li>
                                <li class="list-inline-item">/</li>
                                <li class="list-inline-item">
                                    <i class="icon-finance-206 u-line-icon-pro align-middle g-pos-rel g-top-1 g-mr-2"></i>
                                    52
                                </li>
                                <li class="list-inline-item">/</li>
                                <li class="list-inline-item">
                                    <i class="icon-share g-pos-rel g-top-1 g-mr-2"></i> 26
                                </li>
                            </ul>
                        </div>
                    </article>
                    <!-- End Article -->
                </div>

                <div class="col-lg-6 g-pl-1--lg g-mb-30 g-mb-2--lg">
                    <!-- Article -->
                    <article class="u-block-hover">
                        <figure class="u-shadow-v25 g-bg-cover g-bg-white-gradient-opacity-v1--after">
                            <img class="g-height-300 img-fluid w-100 u-block-hover__main--zoom-v1"
                                src="https://dispaperkan.wonosobokab.go.id/wp-content/uploads/2021/04/WhatsApp-Image-2021-04-29-at-08.47.45-1-768x527.jpeg"
                                alt="Image Description">
                        </figure>

                        <span
                            class="g-hidden-xs-down u-icon-v3 u-icon-size--sm g-font-size-13 g-bg-white rounded-circle g-pos-abs g-top-30 g-right-30">
                            <i class="fa fa-play g-left-2"></i>
                        </span>

                        <span class="g-hidden-xs-down g-pos-abs g-top-30 g-left-30">
                            <a class="btn btn-xs u-btn-red text-uppercase rounded-0" href="#">Berita</a>
                        </span>

                        <div class="g-pos-abs g-bottom-30 g-left-30 g-right-30">
                            <small class="g-color-white">
                                <i class="icon-clock g-pos-rel g-top-1 g-mr-2"></i> July 22, 2016
                            </small>

                            <h3 class="h4 g-my-10">
                                <a class="g-color-white g-color-white--hover" href="#">Bupati Wonosobo Melakukan
                                    Penanaman Perdana Talas Beneng</a>
                            </h3>

                            <ul class="g-hidden-xs-down u-list-inline g-font-size-12 g-color-white">
                                <li class="list-inline-item">
                                    <i class="icon-eye g-pos-rel g-top-1 g-mr-2"></i> 127
                                </li>
                                <li class="list-inline-item">/</li>
                                <li class="list-inline-item">
                                    <i class="icon-finance-206 u-line-icon-pro align-middle g-pos-rel g-top-1 g-mr-2"></i>
                                    152
                                </li>
                                <li class="list-inline-item">/</li>
                                <li class="list-inline-item">
                                    <i class="icon-share g-pos-rel g-top-1 g-mr-2"></i> 32
                                </li>
                            </ul>
                        </div>
                    </article>
                    <!-- End Article -->
                </div>

                <div class="col-lg-4 g-pr-1--lg g-mb-30 g-mb-0--lg">
                    <!-- Article -->
                    <article class="u-block-hover">
                        <figure class="u-shadow-v25 u-bg-overlay g-bg-white-gradient-opacity-v1--after">
                            <img class="g-height-200 u-block-hover__main--zoom-v1 img-fluid w-100"
                                src="https://img.beritasatu.com/cache/beritasatu/910x580-2/1610335183.jpg"
                                alt="Image Description">
                        </figure>

                        <div class="w-100 text-center g-absolute-centered g-px-30">
                            <a class="btn btn-xs u-btn-cyan text-uppercase rounded-0" href="#">Agenda</a>
                            <h3 class="h4 g-mt-10">
                                <a class="g-color-white" href="#">Rapat Kerja Nasional (RAKERNAS) Pembangunan
                                    Pertanian Tahun 2021</a>
                            </h3>
                            <small class="g-color-white">
                                <i class="icon-clock g-pos-rel g-top-1 g-mr-2"></i> July 26, 2017
                            </small>
                        </div>
                    </article>
                    <!-- End Article -->
                </div>

                <div class="col-lg-4 g-px-1--lg g-mb-30 g-mb-0--lg">
                    <!-- Article -->
                    <article class="u-block-hover">
                        <figure class="u-shadow-v25 u-bg-overlay g-bg-white-gradient-opacity-v1--after">
                            <img class="g-height-200 u-block-hover__main--zoom-v1 img-fluid w-100"
                                src="https://dispaperkan.wonosobokab.go.id/wp-content/uploads/2020/08/WhatsApp-Image-2020-08-06-at-10.24.33-723x1024.jpeg"
                                alt="Image Description">
                        </figure>

                        <div class="w-100 text-center g-absolute-centered g-px-30">
                            <a class="btn btn-xs u-btn-pink text-uppercase rounded-0" href="#">Berita</a>
                            <h3 class="h4 g-mt-10">
                                <a class="g-color-white" href="#">Ayo Ikut Berpartisipasi!!!</a>
                            </h3>
                            <small class="g-color-white">
                                <i class="icon-clock g-pos-rel g-top-1 g-mr-2"></i> July 18, 2017
                            </small>
                        </div>
                    </article>
                    <!-- End Article -->
                </div>

                <div class="col-lg-4 g-pl-1--lg">
                    <!-- Article -->
                    <article class="u-block-hover">
                        <figure class="u-shadow-v25 u-bg-overlay g-bg-white-gradient-opacity-v1--after">
                            <img class="g-height-200 u-block-hover__main--zoom-v1 img-fluid w-100"
                                src="https://dispaperkan.wonosobokab.go.id/wp-content/uploads/2018/07/WhatsApp-Image-2018-07-11-at-07.58.43-240x300.jpeg"
                                alt="Image Description">
                        </figure>

                        <div class="w-100 text-center g-absolute-centered g-px-30">
                            <a class="btn btn-xs u-btn-primary text-uppercase rounded-0" href="#">Berita</a>
                            <h3 class="h4 g-mt-10">
                                <a class="g-color-white" href="#">Festival Domba Wonosobo dalam rangka Hari Jadi
                                    Kabupaten Wonosobo ke-193</a>
                            </h3>
                            <small class="g-color-white">
                                <i class="icon-clock g-pos-rel g-top-1 g-mr-2"></i> July 5, 2017
                            </small>
                        </div>
                    </article>
                    <!-- End Article -->
                </div>
            </div>
            <!-- News Section -->
        </div>
    </section>
    <!-- End Promo Block -->

    <!-- News Content -->
    <section class="g-pb-100">
        <div class="container">
            <!-- News Section 1 -->
            <div class="row g-mb-60">
                <!-- Articles Content -->
                <div class="col-lg-9 g-mb-50 g-mb-0--lg">
                    <!-- Latest News -->
                    <div class="g-mb-50">
                        <div class="u-heading-v3-1 g-mb-30">
                            <h2
                                class="h5 u-heading-v3__title g-font-primary g-font-weight-700 g-color-gray-dark-v1 text-uppercase g-brd-primary">
                                Latest News</h2>
                        </div>

                        <div class="row">
                            <!-- Article (Leftside) -->
                            <div class="col-lg-7 g-mb-50 g-mb-0--lg">
                                <article>
                                    <figure class="u-shadow-v25 g-pos-rel g-mb-20">
                                        <img class="img-fluid w-100"
                                            src="https://dispaperkan.wonosobokab.go.id/wp-content/uploads/2017/11/rapat-koordinasi.jpg"
                                            alt="Image Description">

                                        <figcaption class="g-pos-abs g-top-20 g-left-20">
                                            <a class="btn btn-xs u-btn-teal text-uppercase rounded-0" href="#">Siaran
                                                Pers</a>
                                        </figcaption>
                                    </figure>

                                    <h3 class="h4 g-mb-10">
                                        <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="#">Rapat
                                            Koordinasi Antisipasi Hama dan Penyakit Tanaman</a>
                                    </h3>

                                    <ul class="list-inline g-color-gray-dark-v4 g-font-size-12">
                                        <li class="list-inline-item">
                                            <a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#">Mike
                                                Coolman</a>
                                        </li>
                                        <li class="list-inline-item">/</li>
                                        <li class="list-inline-item">
                                            July 20, 2017
                                        </li>
                                        <li class="list-inline-item">/</li>
                                        <li class="list-inline-item">
                                            <a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#">
                                                <i
                                                    class="icon-finance-206 u-line-icon-pro align-middle g-pos-rel g-top-1 mr-1"></i>
                                                24
                                            </a>
                                        </li>
                                    </ul>

                                    <p class="g-color-gray-dark-v2">Kementerian Pertanian gelar Rapat Koordinasi
                                        Antisipasi Hama dan Penyakit Tanaman serta Dampak Perubahan Iklim. Hadir
                                        pada kegiatan ini Menteri Pertanian Andi Amran Sulaiman, jajaran Eselon I
                                        Kementan, para kepala dinas provinsi/kabupaten dan para Brigade Hama ,
                                        Penyakit dan Kekeringan.</p>
                                    <a class="g-font-size-12" href="#">Read More..</a>
                                </article>
                            </div>
                            <!-- End Article (Leftside) -->

                            <!-- Article (Rightside) -->
                            <div class="col-lg-5">
                                <!-- Article -->
                                <article class="media">
                                    <a class="d-flex u-shadow-v25 align-self-center mr-3" href="#">
                                        <img class="g-width-80 g-height-80"
                                            src="https://dispaperkan.wonosobokab.go.id/wp-content/uploads/2017/11/jambore-peternakan-1-300x225.jpeg"
                                            alt="Image Description">
                                    </a>

                                    <div class="media-body">
                                        <h3 class="h6">
                                            <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover"
                                                href="#">Jambore Peternakan Nasional Digelar di Cibubur</a>
                                        </h3>

                                        <ul class="u-list-inline g-font-size-12 g-color-gray-dark-v4">
                                            <li class="list-inline-item">
                                                July 20, 2017
                                            </li>
                                            <li class="list-inline-item">/</li>
                                            <li class="list-inline-item">
                                                <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#">
                                                    <i
                                                        class="icon-finance-206 u-line-icon-pro align-middle g-pos-rel g-top-1 mr-1"></i>
                                                    18
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </article>
                                <!-- End Article -->

                                <hr class="g-brd-gray-light-v4 g-my-25">

                                <!-- Article -->
                                <article class="media">
                                    <a class="d-flex u-shadow-v25 align-self-center mr-3" href="#">
                                        <img class="g-width-80 g-height-80"
                                            src="https://dispaperkan.wonosobokab.go.id/wp-content/uploads/2017/08/WhatsApp-Image-2017-08-15-at-09.54.38.jpeg"
                                            alt="Image Description">
                                    </a>

                                    <div class="media-body">
                                        <h3 class="h6">
                                            <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="#">Siaran
                                                Pers Pertama</a>
                                        </h3>

                                        <ul class="u-list-inline g-font-size-12 g-color-gray-dark-v4">
                                            <li class="list-inline-item">
                                                July 16, 2017
                                            </li>
                                            <li class="list-inline-item">/</li>
                                            <li class="list-inline-item">
                                                <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#">
                                                    <i
                                                        class="icon-finance-206 u-line-icon-pro align-middle g-pos-rel g-top-1 mr-1"></i>
                                                    31
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </article>
                                <!-- End Article -->

                                <hr class="g-brd-gray-light-v4 g-my-25">

                                <!-- Article -->
                                <article class="media">
                                    <a class="d-flex u-shadow-v25 align-self-center mr-3" href="#">
                                        <img class="g-width-80 g-height-80"
                                            src="https://dispaperkan.wonosobokab.go.id/wp-content/uploads/2017/10/Petani-Milenial-212x300.jpg"
                                            alt="Image Description">
                                    </a>

                                    <div class="media-body">
                                        <h3 class="h6">
                                            <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover"
                                                href="#">Pertanian Modern Untuk Generasi Milenia</a>
                                        </h3>

                                        <ul class="u-list-inline g-font-size-12 g-color-gray-dark-v4">
                                            <li class="list-inline-item">
                                                July 07, 2017
                                            </li>
                                            <li class="list-inline-item">/</li>
                                            <li class="list-inline-item">
                                                <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#">
                                                    <i
                                                        class="icon-finance-206 u-line-icon-pro align-middle g-pos-rel g-top-1 mr-1"></i>
                                                    24
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </article>
                                <!-- End Article -->

                                <hr class="g-brd-gray-light-v4 g-my-25">

                                <!-- Article -->
                                <article class="media">
                                    <a class="d-flex u-shadow-v25 align-self-center mr-3" href="#">
                                        <img class="g-width-80 g-height-80"
                                            src="https://akcdn.detik.net.id/community/media/visual/2017/07/24/5bacebbe-ef92-457a-8eff-e6633669076e_169.jpg?w=780&q=90"
                                            alt="Image Description">
                                    </a>

                                    <div class="media-body">
                                        <h3 class="h6">
                                            <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="#">Harga
                                                Eceran Tertinggi Beras Mulai Berlaku Tanggal 1
                                                September 2017</a>
                                        </h3>

                                        <ul class="u-list-inline g-font-size-12 g-color-gray-dark-v4">
                                            <li class="list-inline-item">
                                                July 16, 2017
                                            </li>
                                            <li class="list-inline-item">/</li>
                                            <li class="list-inline-item">
                                                <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#">
                                                    <i
                                                        class="icon-finance-206 u-line-icon-pro align-middle g-pos-rel g-top-1 mr-1"></i>
                                                    31
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </article>
                                <!-- End Article -->
                            </div>
                            <!-- End Article (Rightside) -->
                        </div>
                    </div>
                    <!-- End Latest News -->

                    <!-- Recent Videos -->
                    <div class="u-heading-v3-1 g-mb-30">
                        <h2
                            class="h5 u-heading-v3__title g-font-primary g-font-weight-700 g-color-gray-dark-v1 text-uppercase g-brd-primary">
                            Recent Videos</h2>
                    </div>

                    <div class="row">
                        <!-- Article Video -->
                        <div class="col-lg-4 col-sm-6 g-mb-30">
                            <article>
                                <figure class="u-shadow-v25 g-pos-rel g-mb-20">
                                    <img class="g-height-135 img-fluid w-100"
                                        src="https://img.beritasatu.com/cache/beritasatu/910x580-2/1547963603.jpg"
                                        alt="Image Description">

                                    <figcaption class="g-pos-abs g-top-10 g-left-10">
                                        <a class="btn btn-xs u-btn-blue text-uppercase rounded-0" href="#">Perikanan</a>
                                    </figcaption>

                                    <span
                                        class="u-icon-v3 u-icon-size--sm g-font-size-13 g-bg-white g-bg-black--hover g-color-white--hover rounded-circle g-cursor-pointer g-absolute-centered">
                                        <i class="fa fa-play g-left-2"></i>
                                    </span>
                                </figure>

                                <h3 class="g-font-size-16 g-mb-10">
                                    <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="#">Ekspor
                                        Perikanan Indonesia Naik 12% Setiap Tahun</a>
                                </h3>
                            </article>
                        </div>
                        <!-- End Article Video -->

                        <!-- Article Video -->
                        <div class="col-lg-4 col-sm-6 g-mb-30">
                            <article>
                                <figure class="u-shadow-v25 g-pos-rel g-mb-20">
                                    <img class="g-height-135 img-fluid w-100"
                                        src="https://cdn-2.tstatic.net/jogja/foto/bank/images/ilustrasi-ikan-bernilai-ekspor-di-sebuah-tpi.jpg"
                                        alt="Image Description">

                                    <figcaption class="g-pos-abs g-top-10 g-left-10">
                                        <a class="btn btn-xs u-btn-pink text-uppercase rounded-0" href="#">Perikanan</a>
                                    </figcaption>

                                    <span
                                        class="u-icon-v3 u-icon-size--sm g-font-size-13 g-bg-white g-bg-black--hover g-color-white--hover rounded-circle g-cursor-pointer g-absolute-centered">
                                        <i class="fa fa-play g-left-2"></i>
                                    </span>
                                </figure>

                                <h3 class="g-font-size-16 g-mb-10">
                                    <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="#">Ekspor
                                        Produk Perikanan Indonesia Senilai 143 Juta Dolar AS Tembus Pasar Korsel</a>
                                </h3>
                            </article>
                        </div>
                        <!-- End Article Video -->

                        <!-- Article Video -->
                        <div class="col-lg-4 col-sm-6 g-mb-30">
                            <article>
                                <figure class="u-shadow-v25 g-pos-rel g-mb-20">
                                    <img class="g-height-135 img-fluid w-100"
                                        src="https://asset.kompas.com/crops/vg-sz5D0rbRRKTHXLKbyrIHQf10=/0x0:0x0/750x500/data/photo/2015/07/10/1429158shutterstock-112976938p.jpg"
                                        alt="Image Description">

                                    <figcaption class="g-pos-abs g-top-10 g-left-10">
                                        <a class="btn btn-xs u-btn-teal text-uppercase rounded-0" href="#">Pangan</a>
                                    </figcaption>

                                    <span
                                        class="u-icon-v3 u-icon-size--sm g-font-size-13 g-bg-white g-bg-black--hover g-color-white--hover rounded-circle g-cursor-pointer g-absolute-centered">
                                        <i class="fa fa-play g-left-2"></i>
                                    </span>
                                </figure>

                                <h3 class="g-font-size-16 g-mb-10">
                                    <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="#">Global
                                        Food Security Nilai Ketahanan Pangan Indonesia Tertinggi di Dunia</a>
                                </h3>
                            </article>
                        </div>
                        <!-- End Article Video -->

                        <!-- Article Video -->
                        <div class="col-lg-4 col-sm-6 g-mb-30 g-mb-0--sm">
                            <article>
                                <figure class="u-shadow-v25 g-pos-rel g-mb-20">
                                    <img class="g-height-135 img-fluid w-100"
                                        src="https://disk.mediaindonesia.com/thumbs/1800x1200/news/2020/08/8b74fa22d37e4b3b037c69e1720f6636.jpg"
                                        alt="Image Description">

                                    <figcaption class="g-pos-abs g-top-10 g-left-10">
                                        <a class="btn btn-xs u-btn-darkred text-uppercase rounded-0" href="#">Peternakan</a>
                                    </figcaption>

                                    <span
                                        class="u-icon-v3 u-icon-size--sm g-font-size-13 g-bg-white g-bg-black--hover g-color-white--hover rounded-circle g-cursor-pointer g-absolute-centered">
                                        <i class="fa fa-play g-left-2"></i>
                                    </span>
                                </figure>

                                <h3 class="g-font-size-16 g-mb-10">
                                    <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="#">Peternak Sapi
                                        di Riau Tetap Untung di Era Kenormalan Baru</a>
                                </h3>
                            </article>
                        </div>
                        <!-- End Article Video -->

                        <!-- Article Video -->
                        <div class="col-lg-4 col-sm-6 g-mb-30 g-mb-0--sm">
                            <article>
                                <figure class="u-shadow-v25 g-pos-rel g-mb-20">
                                    <img class="g-height-135 img-fluid w-100"
                                        src="https://media.suara.com/pictures/970x544/2019/05/07/13508-kementan.jpg"
                                        alt="Image Description">

                                    <figcaption class="g-pos-abs g-top-10 g-left-10">
                                        <a class="btn btn-xs u-btn-indigo text-uppercase rounded-0" href="#">Peternakan</a>
                                    </figcaption>

                                    <span
                                        class="u-icon-v3 u-icon-size--sm g-font-size-13 g-bg-white g-bg-black--hover g-color-white--hover rounded-circle g-cursor-pointer g-absolute-centered">
                                        <i class="fa fa-play g-left-2"></i>
                                    </span>
                                </figure>

                                <h3 class="g-font-size-16 g-mb-10">
                                    <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="#">Realisasi
                                        Premi Asuransi Ternak Sapi hingga Juli 2019 Capai Rp
                                        17,48 M</a>
                                </h3>
                            </article>
                        </div>
                        <!-- End Article Video -->

                        <!-- Article Video -->
                        <div class="col-lg-4 col-sm-6">
                            <article>
                                <figure class="u-shadow-v25 g-pos-rel g-mb-20">
                                    <img class="g-height-135 img-fluid w-100"
                                        src="https://disk.mediaindonesia.com/thumbs/1800x1200/news/2017/09/ayam-.jpg"
                                        alt="Image Description">

                                    <figcaption class="g-pos-abs g-top-10 g-left-10">
                                        <a class="btn btn-xs u-btn-brown text-uppercase rounded-0" href="#">Peternakan</a>
                                    </figcaption>

                                    <span
                                        class="u-icon-v3 u-icon-size--sm g-font-size-13 g-bg-white g-bg-black--hover g-color-white--hover rounded-circle g-cursor-pointer g-absolute-centered">
                                        <i class="fa fa-play g-left-2"></i>
                                    </span>
                                </figure>

                                <h3 class="g-font-size-16 g-mb-10">
                                    <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="#">Sektor
                                        Peternakan Harus Terintegrasi</a>
                                </h3>
                            </article>
                        </div>
                        <!-- End Article Video -->
                    </div>
                    <!-- End Recent Videos -->

                    <div id="stickyblock-end"></div>
                </div>
                <!-- End Articles Content -->

                <!-- Sidebar -->
                <div class="col-lg-3">
                    <!-- Useful Links -->
                    <div class="g-mb-50">
                        <div class="u-heading-v3-1 g-mb-30">
                            <h2
                                class="h5 u-heading-v3__title g-font-primary g-font-weight-700 g-color-gray-dark-v1 text-uppercase g-brd-primary">
                                Kategori</h2>
                        </div>

                        <ul class="list-unstyled">
                            <li class="g-brd-bottom g-brd-gray-light-v4 g-pb-10 g-mb-12">
                                <h4 class="h6">
                                    <i class="fa fa-angle-right g-color-gray-dark-v5 g-mr-5"></i>
                                    <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="#">Berita
                                        Populer</a>
                                </h4>
                            </li>
                            <li class="g-brd-bottom g-brd-gray-light-v4 g-pb-10 g-mb-12">
                                <h4 class="h6">
                                    <i class="fa fa-angle-right g-color-gray-dark-v5 g-mr-5"></i>
                                    <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="#">Siaran
                                        Pers</a>
                                </h4>
                            </li>
                            <li class="g-brd-bottom g-brd-gray-light-v4 g-pb-10 g-mb-12">
                                <h4 class="h6">
                                    <i class="fa fa-angle-right g-color-gray-dark-v5 g-mr-5"></i>
                                    <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="#">Media
                                        Lain</a>
                                </h4>
                            </li>
                            <li class="g-brd-bottom g-brd-gray-light-v4 g-pb-10 g-mb-12">
                                <h4 class="h6">
                                    <i class="fa fa-angle-right g-color-gray-dark-v5 g-mr-5"></i>
                                    <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="#">Pengumuman</a>
                                </h4>
                            </li>
                            <li class="g-brd-bottom g-brd-gray-light-v4 g-pb-10 g-mb-12">
                                <h4 class="h6">
                                    <i class="fa fa-angle-right g-color-gray-dark-v5 g-mr-5"></i>
                                    <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="#">Agenda</a>
                                </h4>
                            </li>
                            <li class="mb-0">
                                <h4 class="h6">
                                    <i class="fa fa-angle-right g-color-gray-dark-v5 g-mr-5"></i>
                                    <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="#">Berita
                                        Iptek</a>
                                </h4>
                            </li>
                        </ul>
                    </div>
                    <!-- End Useful Links -->

                    <div id="stickyblock-start" class="js-sticky-block g-sticky-block--lg g-pt-20"
                        data-start-point="#stickyblock-start" data-end-point="#stickyblock-end">

                        <!-- Social Links -->
                        <div class="g-mb-50">
                            <div class="u-heading-v3-1 g-mb-30">
                                <h2
                                    class="h5 u-heading-v3__title g-font-primary g-font-weight-700 g-color-gray-dark-v1 text-uppercase g-brd-primary">
                                    Social Links</h2>
                            </div>

                            <ul class="list-unstyled mb-0">
                                <li class="d-flex align-items-center justify-content-between g-mb-20">
                                    <a class="d-flex align-items-center u-link-v5 g-color-gray-dark-v1 g-color-primary--hover"
                                        href="#">
                                        <span
                                            class="u-icon-v3 u-icon-size--xs u-shadow-v25 g-font-size-12 g-bg-facebook g-bg-facebook--hover g-color-white rounded-circle g-mr-10"
                                            href="#">
                                            <i class="fa fa-facebook"></i>
                                        </span>
                                        Like
                                    </a>
                                    <span class="js-counter d-block g-color-gray-dark-v4">103832</span>
                                </li>
                                <li class="d-flex align-items-center justify-content-between g-mb-20">
                                    <a class="d-flex align-items-center u-link-v5 g-color-gray-dark-v1 g-color-primary--hover"
                                        href="#">
                                        <span
                                            class="u-icon-v3 u-icon-size--xs u-shadow-v25 g-font-size-12 g-bg-lightred g-bg-lightred--hover g-color-white rounded-circle g-mr-10"
                                            href="#">
                                            <i class="fa fa-google-plus"></i>
                                        </span>
                                        Add to Circle
                                    </a>
                                    <span class="js-counter d-block g-color-gray-dark-v4">47192</span>
                                </li>
                                <li class="d-flex align-items-center justify-content-between g-mb-20">
                                    <a class="d-flex align-items-center u-link-v5 g-color-gray-dark-v1 g-color-primary--hover"
                                        href="#">
                                        <span
                                            class="u-icon-v3 u-icon-size--xs u-shadow-v25 g-font-size-12 g-bg-instagram g-bg-instagram--hover g-color-white rounded-circle g-mr-10"
                                            href="#">
                                            <i class="fa fa-instagram"></i>
                                        </span>
                                        Follow Us
                                    </a>
                                    <span class="js-counter d-block g-color-gray-dark-v4">38291</span>
                                </li>
                                <li class="d-flex align-items-center justify-content-between g-mb-20">
                                    <a class="d-flex align-items-center u-link-v5 g-color-gray-dark-v1 g-color-primary--hover"
                                        href="#">
                                        <span
                                            class="u-icon-v3 u-icon-size--xs u-shadow-v25 g-font-size-12 g-bg-teal g-bg-teal--hover g-color-white rounded-circle g-mr-10"
                                            href="#">
                                            <i class="fa fa-medium"></i>
                                        </span>
                                        Writers
                                    </a>
                                    <span class="js-counter d-block g-color-gray-dark-v4">23871</span>
                                </li>
                                <li class="d-flex align-items-center justify-content-between">
                                    <a class="d-flex align-items-center u-link-v5 g-color-gray-dark-v1 g-color-primary--hover"
                                        href="#">
                                        <span
                                            class="u-icon-v3 u-icon-size--xs u-shadow-v25 g-font-size-12 g-bg-twitter g-bg-twitter--hover g-color-white rounded-circle g-mr-10"
                                            href="#">
                                            <i class="fa fa-twitter"></i>
                                        </span>
                                        Follow Us
                                    </a>
                                    <span class="js-counter d-block g-color-gray-dark-v4">391743</span>
                                </li>
                            </ul>
                        </div>
                        <!-- End Social Links -->
                    </div>
                </div>
                <!-- End Sidebar -->
            </div>
            <!-- News Section 1 -->

            <!-- News Section 2 -->
            <div class="row no-gutters g-mb-60">
                <div class="col-lg-4 g-pr-2--lg g-mb-30 g-mb-0--lg">
                    <!-- Article -->
                    <article class="u-block-hover">
                        <figure class="u-shadow-v25 u-bg-overlay g-bg-white-gradient-opacity-v1--after">
                            <img class="u-block-hover__main--zoom-v1 img-fluid"
                                src="https://htmlstream.com/preview/unify-v2.6.3/multipage/blog-magazine/classic/assets/img-temp/500x450/img5.jpg"
                                alt="Image Description">
                        </figure>

                        <div class="w-100 text-center g-absolute-centered g-px-20">
                            <a class="btn btn-xs u-btn-deeporange text-uppercase rounded-0" href="#">Fotografi</a>
                            <h3 class="h4 g-mt-10">
                                <a class="g-color-white" href="#">Pengembangan fotografi pada kaum milenial</a>
                            </h3>
                            <small class="g-color-white">
                                <i class="icon-clock g-pos-rel g-top-1 g-mr-2"></i> July 26, 2017
                            </small>
                        </div>
                    </article>
                    <!-- End Article -->
                </div>

                <div class="col-lg-4 g-px-1--lg g-mb-30 g-mb-0--lg">
                    <!-- Article -->
                    <article class="u-block-hover">
                        <figure class="u-shadow-v25 u-bg-overlay g-bg-white-gradient-opacity-v1--after">
                            <img class="u-block-hover__main--zoom-v1 img-fluid"
                                src="https://htmlstream.com/preview/unify-v2.6.3/multipage/blog-magazine/classic/assets/img-temp/500x450/img6.jpg"
                                alt="Image Description">
                        </figure>
                        <div class="w-100 text-center g-absolute-centered g-px-20">
                            <a class="btn btn-xs u-btn-cyan text-uppercase rounded-0" href="#">Lingkungan</a>
                            <h3 class="h4 g-mt-10">
                                <a class="g-color-white" href="#">Penyelamatan lingkungan hidup diwonosobo</a>
                            </h3>
                            <small class="g-color-white">
                                <i class="icon-clock g-pos-rel g-top-1 g-mr-2"></i> July 18, 2017
                            </small>
                        </div>
                    </article>
                    <!-- End Article -->
                </div>

                <div class="col-lg-4 g-pl-2--lg">
                    <!-- Article -->
                    <article class="u-block-hover">
                        <figure class="u-shadow-v25 u-bg-overlay g-bg-white-gradient-opacity-v1--after">
                            <img class="u-block-hover__main--zoom-v1 img-fluid"
                                src="https://htmlstream.com/preview/unify-v2.6.3/multipage/blog-magazine/classic/assets/img-temp/500x450/img7.jpg"
                                alt="Image Description">
                        </figure>
                        <div class="w-100 text-center g-absolute-centered g-px-20">
                            <a class="btn btn-xs u-btn-darkred text-uppercase rounded-0" href="#">Office</a>
                            <h3 class="h4 g-mt-10">
                                <a class="g-color-white" href="#">Lowker satpam tahun 2022</a>
                            </h3>
                            <small class="g-color-white">
                                <i class="icon-clock g-pos-rel g-top-1 g-mr-2"></i> July 5, 2017
                            </small>
                        </div>
                    </article>
                    <!-- End Article -->
                </div>
            </div>
            <!-- News Section 2 -->

            <!-- News Section 3 -->
            <div class="row">
                <!-- Articles Content -->
                <div class="col-lg-9 g-mb-50 g-mb-0--lg">
                    <!-- Popular News -->
                    <div class="g-mb-60">
                        <div class="u-heading-v3-1 g-mb-30">
                            <h2
                                class="h5 u-heading-v3__title g-font-primary g-font-weight-700 g-color-gray-dark-v1 text-uppercase g-brd-primary">
                                Popular News</h2>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 g-mb-50 g-mb-0--lg">
                                <!-- Article -->
                                <article class="g-mb-40">
                                    <figure class="u-shadow-v25 g-pos-rel g-mb-20">
                                        <img class="img-fluid w-100"
                                            src="https://static.republika.co.id/uploads/images/inpicture_slide/nelayan-menjemur-ikan-asin-di-pelabuhan-perikanan-nusantara-ppn_200315171131-112.jpg"
                                            alt="Image Description">

                                        <figcaption class="g-pos-abs g-top-20 g-left-20">
                                            <a class="btn btn-xs u-btn-teal text-uppercase rounded-0" href="#">Perikanan</a>
                                        </figcaption>
                                    </figure>

                                    <h3 class="h4 g-mb-10">
                                        <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="#">KKP
                                            Siapkan Antisipasi Dampak Covid-19 Bagi Perikanan |
                                            Republika Online</a>
                                    </h3>

                                    <ul class="list-inline g-color-gray-dark-v4 g-font-size-12">
                                        <li class="list-inline-item">
                                            <a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#">Mike
                                                Coolman</a>
                                        </li>
                                        <li class="list-inline-item">/</li>
                                        <li class="list-inline-item">
                                            July 20, 2017
                                        </li>
                                        <li class="list-inline-item">/</li>
                                        <li class="list-inline-item">
                                            <a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#">
                                                <i
                                                    class="icon-finance-206 u-line-icon-pro align-middle g-pos-rel g-top-1 mr-1"></i>
                                                24
                                            </a>
                                        </li>
                                    </ul>

                                    <p class="g-color-gray-dark-v2">KKP siapakan antisipasi dampak covid yang
                                        terjadi</p>
                                    <a class="g-font-size-12" href="#">Read More..</a>
                                </article>
                                <!-- Article -->
                            </div>

                        </div>
                    </div>
                    <!-- End Popular News -->

                    <!-- Pagination -->
                    <nav id="stickyblock-end-1" aria-label="Page Navigation">
                        <ul class="list-inline text-center mb-0">
                            <li class="list-inline-item">
                                <a class="active u-pagination-v1__item g-width-30 g-height-30 g-brd-secondary-light-v2 g-brd-primary--active g-color-white g-bg-primary--active g-font-size-12 rounded g-pa-5"
                                    href="#">1</a>
                            </li>
                            <li class="list-inline-item">
                                <a class="u-pagination-v1__item g-width-30 g-height-30 g-brd-transparent g-brd-primary--hover g-brd-primary--active g-color-secondary-dark-v1 g-bg-primary--active g-font-size-12 rounded g-pa-5"
                                    href="#">2</a>
                            </li>
                            <li class="list-inline-item g-hidden-xs-down">
                                <a class="u-pagination-v1__item g-width-30 g-height-30 g-brd-transparent g-brd-primary--hover g-brd-primary--active g-color-secondary-dark-v1 g-bg-primary--active g-font-size-12 rounded g-pa-5"
                                    href="#">3</a>
                            </li>
                            <li class="list-inline-item">
                                <span
                                    class="g-width-30 g-height-30 g-color-gray-dark-v5 g-font-size-12 rounded g-pa-5">...</span>
                            </li>
                            <li class="list-inline-item g-hidden-xs-down">
                                <a class="u-pagination-v1__item g-width-30 g-height-30 g-brd-transparent g-brd-primary--hover g-brd-primary--active g-color-secondary-dark-v1 g-bg-primary--active g-font-size-12 rounded g-pa-5"
                                    href="#">15</a>
                            </li>
                            <li class="list-inline-item">
                                <a class="u-pagination-v1__item g-brd-secondary-light-v2 g-brd-primary--hover g-color-gray-dark-v5 g-color-primary--hover g-font-size-12 rounded g-px-15 g-py-5 g-ml-15"
                                    href="#" aria-label="Next">
                                    <span aria-hidden="true">
                                        Next
                                        <i class="ml-2 fa fa-angle-right"></i>
                                    </span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <!-- End Pagination -->
                </div>
                <!-- End Articles -->

                <!-- Sidebar -->
                <div class="col-lg-3">
                    <!-- Popular Tags -->
                    <div class="g-mb-20">
                        <div class="u-heading-v3-1 g-mb-30">
                            <h2
                                class="h5 u-heading-v3__title g-font-primary g-font-weight-700 g-color-gray-dark-v1 text-uppercase g-brd-primary">
                                Popular Tags</h2>
                        </div>

                        <ul class="u-list-inline g-font-size-11 text-uppercase mb-0">
                            <li class="list-inline-item g-mb-10">
                                <a class="u-tags-v1 g-brd-around g-brd-gray-light-v4 g-bg-primary--hover g-brd-primary--hover g-color-black-opacity-0_8 g-color-white--hover rounded g-py-6 g-px-15"
                                    href="#">Agenda</a>
                            </li>
                            <li class="list-inline-item g-mb-10">
                                <a class="u-tags-v1 g-brd-around g-brd-gray-light-v4 g-bg-primary--hover g-brd-primary--hover g-color-black-opacity-0_8 g-color-white--hover rounded g-py-6 g-px-15"
                                    href="#">Lingkunga</a>
                            </li>
                            <li class="list-inline-item g-mb-10">
                                <a class="u-tags-v1 g-brd-around g-brd-gray-light-v4 g-bg-primary--hover g-brd-primary--hover g-color-black-opacity-0_8 g-color-white--hover rounded g-py-6 g-px-15"
                                    href="#">Perikanan</a>
                            </li>
                            <li class="list-inline-item g-mb-10">
                                <a class="u-tags-v1 g-brd-around g-brd-gray-light-v4 g-bg-primary--hover g-brd-primary--hover g-color-black-opacity-0_8 g-color-white--hover rounded g-py-6 g-px-15"
                                    href="#">Pangan</a>
                            </li>
                            <li class="list-inline-item g-mb-10">
                                <a class="u-tags-v1 g-brd-around g-brd-gray-light-v4 g-bg-primary--hover g-brd-primary--hover g-color-black-opacity-0_8 g-color-white--hover rounded g-py-6 g-px-15"
                                    href="#">Berita</a>
                            </li>
                            <li class="list-inline-item g-mb-10">
                                <a class="u-tags-v1 g-brd-around g-brd-gray-light-v4 g-bg-primary--hover g-brd-primary--hover g-color-black-opacity-0_8 g-color-white--hover rounded g-py-6 g-px-15"
                                    href="#">Pertanian</a>
                            </li>
                            <li class="list-inline-item g-mb-10">
                                <a class="u-tags-v1 g-brd-around g-brd-gray-light-v4 g-bg-primary--hover g-brd-primary--hover g-color-black-opacity-0_8 g-color-white--hover rounded g-py-6 g-px-15"
                                    href="#">Peternakan</a>
                            </li>
                            <li class="list-inline-item g-mb-10">
                                <a class="u-tags-v1 g-brd-around g-brd-gray-light-v4 g-bg-primary--hover g-brd-primary--hover g-color-black-opacity-0_8 g-color-white--hover rounded g-py-6 g-px-15"
                                    href="#">Office</a>
                            </li>
                        </ul>
                    </div>
                    <!-- End Popular Tags -->

                    <div id="stickyblock-start-1" class="js-sticky-block g-sticky-block--lg g-pt-20"
                        data-start-point="#stickyblock-start-1" data-end-point="#stickyblock-end-1">
                        <!-- Top Authors -->
                        <div class="g-mb-40">
                            <div class="u-heading-v3-1 g-mb-30">
                                <h2
                                    class="h5 u-heading-v3__title g-font-primary g-font-weight-700 g-color-gray-dark-v1 text-uppercase g-brd-primary">
                                    Top Authors</h2>
                            </div>

                            <article class="media g-mb-35">
                                <img class="d-flex u-shadow-v25 g-width-40 g-height-40 rounded-circle mr-3"
                                    src="https://htmlstream.com/preview/unify-v2.6.3/multipage/blog-magazine/classic/assets/img-temp/100x100/img1.jpg"
                                    alt="Image Description">
                                <div class="media-body">
                                    <h4 class="g-font-size-16">
                                        <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="#">Super
                                            Admin</a>
                                        <small class="g-color-gray-dark-v4">@jokopambudi</small>
                                    </h4>
                                    <p class="g-color-gray-dark-v2">Menyampaikan kebenaran walau setetes air.</p>
                                    <a class="btn u-btn-outline-primary g-font-size-11 text-uppercase" href="#">Follow</a>
                                </div>
                            </article>

                            <article class="media g-mb-35">
                                <img class="d-flex u-shadow-v25 g-width-40 g-height-40 rounded-circle mr-3"
                                    src="https://htmlstream.com/preview/unify-v2.6.3/multipage/blog-magazine/classic/assets/img-temp/100x100/img3.jpg"
                                    alt="Image Description">
                                <div class="media-body">
                                    <h4 class="g-font-size-16 g-color-gray-dark-v1">
                                        <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="#">Pixeel</a>
                                        <small class="g-color-gray-dark-v4">@ahmadishaq</small>
                                    </h4>
                                    <p class="g-color-gray-dark-v2">Kerja kerja kerja.</p>
                                    <a class="btn u-btn-outline-primary g-font-size-11 text-uppercase" href="#">Follow</a>
                                </div>
                            </article>
                        </div>
                        <!-- End Top Authors -->
                    </div>
                </div>
                <!-- End Sidebar -->
            </div>
            <!-- News Section 3 -->
        </div>
    </section>
    <!-- End News Content -->

@stop
