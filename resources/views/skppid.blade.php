@extends('layout.master')
@section('content')
    <!-- Breadcrumbs -->
    <section class="g-bg-gray-light-v5 g-py-50">
        <div class="container">
            <div class="d-sm-flex text-center">
                <div class="align-self-center">
                    <h2 class="h3 g-font-weight-300 w-100 g-mb-10 g-mb-0--md">SK PPID</h2>
                </div>

                <div class="align-self-center ml-auto">
                    <ul class="u-list-inline">
                        <li class="list-inline-item g-mr-5">
                            <a class="u-link-v5 g-color-main g-color-primary--hover" href="#">PPID</a>
                            <i class="g-color-gray-light-v2 g-ml-5">/</i>
                        </li>
                        <li class="list-inline-item g-color-primary">
                            <span>SK PPID</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- End Breadcrumbs -->


    <!-- Portfolio Single Item -->
    <section class="container g-py-50">
        <div class="row g-mb-50">
            <div class="col-md-8 g-mb-30">
                <!-- Article -->
                <article class="d-md-table w-100 g-bg-white g-mb-1">
                    <!-- Date -->
                    <div
                        class="d-md-table-cell align-middle g-width-125--md text-center g-color-gray-dark-v5 g-py-10 g-px-20">
                        <time datetime="2015-06-27">
                            <span class="d-block g-color-black g-font-weight-700 g-font-size-40 g-line-height-1">27</span>
                            Jun, 2015
                        </time>
                    </div>
                    <!-- End Date -->

                    <!-- Article Content -->
                    <div class="d-md-table-cell align-middle g-py-15 g-px-20">
                        <h3 class="h6 g-font-weight-700 text-uppercase">
                            <a class="g-color-gray-dark-v2" href="#!">SK PPID Dispaperkan download disini</a>
                        </h3>
                        <em class="g-color-gray-dark-v5 g-font-style-normal">Wonosobo, 8 Jun 2014 18:00</em>
                    </div>
                    <!-- End Article Content -->

                    <!-- Actions -->
                    <div class="d-md-table-cell align-middle text-md-right g-pa-20">
                        <div class="g-mt-minus-10 g-mx-minus-5">
                            <a class="btn btn-lg u-btn-primary g-font-weight-600 g-font-size-12 text-uppercase g-mx-5 g-mt-10"
                                href="#!">Download</a>
                        </div>
                    </div>
                    <!-- End Actions -->
                </article>
                <!-- End Article -->
                <!-- Article -->
                <article class="d-md-table w-100 g-bg-white g-mb-1">
                    <!-- Date -->
                    <div
                        class="d-md-table-cell align-middle g-width-125--md text-center g-color-gray-dark-v5 g-py-10 g-px-20">
                        <time datetime="2015-06-27">
                            <span class="d-block g-color-black g-font-weight-700 g-font-size-40 g-line-height-1">27</span>
                            Jun, 2015
                        </time>
                    </div>
                    <!-- End Date -->

                    <!-- Article Content -->
                    <div class="d-md-table-cell align-middle g-py-15 g-px-20">
                        <h3 class="h6 g-font-weight-700 text-uppercase">
                            <a class="g-color-gray-dark-v2" href="#!">SK PPID Dispaperkan download disini</a>
                        </h3>
                        <em class="g-color-gray-dark-v5 g-font-style-normal">Wonosobo, 8 Jun 2014 18:00</em>
                    </div>
                    <!-- End Article Content -->

                    <!-- Actions -->
                    <div class="d-md-table-cell align-middle text-md-right g-pa-20">
                        <div class="g-mt-minus-10 g-mx-minus-5">
                            <a class="btn btn-lg u-btn-primary g-font-weight-600 g-font-size-12 text-uppercase g-mx-5 g-mt-10"
                                href="#!">Download</a>
                        </div>
                    </div>
                    <!-- End Actions -->
                </article>
                <!-- End Article -->
                <!-- Article -->
                <article class="d-md-table w-100 g-bg-white g-mb-1">
                    <!-- Date -->
                    <div
                        class="d-md-table-cell align-middle g-width-125--md text-center g-color-gray-dark-v5 g-py-10 g-px-20">
                        <time datetime="2015-06-27">
                            <span class="d-block g-color-black g-font-weight-700 g-font-size-40 g-line-height-1">27</span>
                            Jun, 2015
                        </time>
                    </div>
                    <!-- End Date -->

                    <!-- Article Content -->
                    <div class="d-md-table-cell align-middle g-py-15 g-px-20">
                        <h3 class="h6 g-font-weight-700 text-uppercase">
                            <a class="g-color-gray-dark-v2" href="#!">SK PPID Dispaperkan download disini</a>
                        </h3>
                        <em class="g-color-gray-dark-v5 g-font-style-normal">Wonosobo, 8 Jun 2014 18:00</em>
                    </div>
                    <!-- End Article Content -->

                    <!-- Actions -->
                    <div class="d-md-table-cell align-middle text-md-right g-pa-20">
                        <div class="g-mt-minus-10 g-mx-minus-5">
                            <a class="btn btn-lg u-btn-primary g-font-weight-600 g-font-size-12 text-uppercase g-mx-5 g-mt-10"
                                href="#!">Download</a>
                        </div>
                    </div>
                    <!-- End Actions -->
                </article>
                <!-- End Article -->
            </div>

            <div class="col-md-4 g-mb-30">
                <!-- Client -->
                <div class="mb-5">
                    <h3 class="h5 g-color-black mb-3">Client:</h3>
                    <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#">
                        <img class="g-width-25 g-height-25 rounded-circle mr-2 mb-1"
                            src="../../assets/img-temp/100x100/img1.jpg" alt="Image Description">Htmlstream
                    </a>
                </div>
                <!-- End Client -->

                <!-- Designers -->
                <div class="mb-5">
                    <h3 class="h5 g-color-black mb-3">Designers:</h3>
                    <ul class="list-unstyled">
                        <li class="my-3">
                            <img class="g-width-25 g-height-25 rounded-circle mb-1 mr-2"
                                src="../../assets/img-temp/100x100/img7.jpg" alt="Image Description">
                            <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#">Alex Teseira</a>
                        </li>
                    </ul>
                </div>
                <!-- End Designers -->

                <!-- Tags -->
                <div class="g-mb-30">
                    <h3 class="h5 g-color-black mb-3">Tags:</h3>
                    <ul class="u-list-inline mb-0">
                        <li class="list-inline-item g-mb-10">
                            <a class="u-tags-v1 g-color-main g-brd-around g-brd-gray-light-v3 g-bg-gray-dark-v2--hover g-brd-gray-dark-v2--hover g-color-white--hover g-rounded-50 g-py-4 g-px-15"
                                href="#">Design</a>
                        </li>
                        <li class="list-inline-item g-mb-10">
                            <a class="u-tags-v1 g-color-main g-brd-around g-brd-gray-light-v3 g-bg-gray-dark-v2--hover g-brd-gray-dark-v2--hover g-color-white--hover g-rounded-50 g-py-4 g-px-15"
                                href="#">Art</a>
                        </li>
                        <li class="list-inline-item g-mb-10">
                            <a class="u-tags-v1 g-color-main g-brd-around g-brd-gray-light-v3 g-bg-gray-dark-v2--hover g-brd-gray-dark-v2--hover g-color-white--hover g-rounded-50 g-py-4 g-px-15"
                                href="#">Graphic</a>
                        </li>
                    </ul>
                </div>
                <!-- End Tags -->

                <!-- Share -->
                <div class="mb-3">
                    <h3 class="h5 g-color-black mb-3">Share:</h3>
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item mr-1 mb-1">
                            <a class="btn u-btn-outline-facebook g-rounded-25" href="#">
                                <i class="mr-1 fa fa-facebook"></i>
                                Facebook
                            </a>
                        </li>
                        <li class="list-inline-item mx-1 mb-1">
                            <a class="btn u-btn-outline-twitter g-rounded-25" href="#">
                                <i class="mr-1 fa fa-twitter"></i>
                                Twitter
                            </a>
                        </li>
                        <li class="list-inline-item mx-1 mb-1">
                            <a class="btn u-btn-outline-dribbble g-rounded-25" href="#">
                                <i class="mr-1 fa fa-dribbble"></i>
                                Dribbble
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- End Share -->
            </div>
        </div>
    </section>
    <!-- End Portfolio Single Item -->
@stop
